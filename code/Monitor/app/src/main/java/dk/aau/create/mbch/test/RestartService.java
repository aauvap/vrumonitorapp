package dk.aau.create.mbch.test;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class RestartService extends Service {
    public static boolean active;

    public final static String EXTRA_ID = "extraID";
    public final static String EXTRA_MODE = "extraMode";
    public final static String EXTRA_ERROR_NOTIFICATION = "extraErrorNotification";


    public RestartService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String id = intent.getStringExtra(EXTRA_ID);
        int mode = intent.getIntExtra(EXTRA_MODE, 0);
        boolean errorNotification = intent.getBooleanExtra(EXTRA_ERROR_NOTIFICATION, false);

        if(active) {
            if(!AccService.isRunning()) {
                Intent restartMyservice = new Intent(this, AccService.class)
                        .putExtra(AccService.EXTRA_ID, id)
                        .putExtra(AccService.EXTRA_MODE, mode)
                        .putExtra(AccService.EXTRA_ERRORNOTIFICATION, errorNotification);

                startService(restartMyservice);
            }



            PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, flags);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, 60000, pendingIntent);
        }

        stopSelf();

        return super.onStartCommand(intent, flags, startId);
    }
}