package dk.aau.create.mbch.test;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AccService extends Service implements SensorEventListener {

    private static boolean isRunning = false;
    private static String identifier = "";
    private static String mode = "";
    private static boolean errorNotifications;
    private static AccService instance;

    private SensorManager sensorMan;
    private Sensor linAcc;
    private Sensor gyro;
    private Sensor geoMat;
    private Sensor acc;

    @SuppressWarnings("FieldCanBeLocal")
    private final int samplingFreq = SensorManager.SENSOR_DELAY_NORMAL;

    private final int notificationIcon = R.drawable.ic_launcher;
    private final int notificationID = 0;
    private PendingIntent notificationIntent;

    public static final String linAccFileName = "VRUlinAccOutput";
    public static final String gyroFileName = "VRUgyroOutput";
    public static final String orientationFileName = "VRUorientationOutput";
    private final String errorFileName = "ErrorLog ";

    private final String header = "Date,NanoTime,Identifier,Mode,X,Y,Z,msDifference,speed,screenOn,latitude,longitude\n";
    private final String errorHeader = "Date,NanoTime,Identifier,Sensor,pauseLength\n";

    private long lastLinAcc = 0L, lastGyro = 0L, lastOrientation = 0l;
    private final long delay = 1000000000L * 5L;
    private SaveDataThread  saveDataThread = new SaveDataThread();

    public static final String EXTRA_ID = "ID";
    public static final String EXTRA_MODE = "Mode";
    public static final String EXTRA_ERRORNOTIFICATION = "errorNotification";

    private float speed = 0;

    private final ArrayList<SensorEventClone> sensorEventQueue = new ArrayList<>();

    private PowerManager.WakeLock wl;

    private PowerManager powerManager;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private float[] mGravity;
    private float[] mGeomagnetic;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        instance = this;
        isRunning = true;
        if(intent == null) {
            saveDataToFile(errorFileName, "nullIntent", errorHeader);
            stopSelf();
        } else {
            identifier = intent.getStringExtra(EXTRA_ID);
            mode = intent.getStringExtra(EXTRA_MODE);
            errorNotifications = intent.getBooleanExtra(EXTRA_ERRORNOTIFICATION, false);

            registerListener();
            Notification runningNotification = createRunnningNotification();
            startForeground(1337, runningNotification);

            saveDataThread.start();

            powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            wl = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "lockTag");
            wl.acquire();
        }

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationListener = new speed();
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (SecurityException e) {

        }

        return START_STICKY;
    }

    private Location lastLocation = null;

    class speed implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            if(lastLocation != null) {
                float distance = lastLocation.distanceTo(loc);
                speed = distance / ((loc.getTime()-lastLocation.getTime())/1000);
            }

            lastLocation = loc;
        }
        @Override
        public void onProviderDisabled(String arg0) {}
        @Override
        public void onProviderEnabled(String arg0) {}
        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}

    }

    /**
     * A function that registers the service to the sensorEventListener, and therby de-facto starts reading the sensors
     */
    private void registerListener() {
        sensorMan = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        linAcc = sensorMan.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gyro = sensorMan.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        geoMat = sensorMan.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        acc = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        final SensorEventListener listener = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();

                Handler handler = new Handler();

                sensorMan.registerListener(listener, linAcc,samplingFreq, handler);
                sensorMan.registerListener(listener, gyro, samplingFreq, handler);
                sensorMan.registerListener(listener, geoMat, samplingFreq, handler);
                sensorMan.registerListener(listener, acc, samplingFreq, handler);

                Looper.loop();
            }
        }).start();
    }

    /**
     * Notifies the user that the service is running
     */
    private Notification createRunnningNotification() {
        Intent notIntent = new Intent(this, MainActivity.class);
        notificationIntent = PendingIntent.getActivity(this, 0, notIntent, 0);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(notificationID);
        NotificationCompat.Builder notificationMainBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.notification_running_header))
                .setSmallIcon(notificationIcon)
                .setContentText(getResources().getString(R.string.notification_running_text))
                .setOngoing(true)
                .setContentIntent(notificationIntent);

        //((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(notificationID, notificationOnStart);
        return notificationMainBuilder.build();
    }

    /**
     * Notifies the user that the service has stopped
     */
    private void createStoppedNotification() {
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(1337);
        long[] vibratePattern = {0, 1000};
        Notification notificationOnStop = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.notification_stopped_header))
                .setSmallIcon(notificationIcon)
                .setContentText(getResources().getString(R.string.notification_stopped_text))
                .setVibrate(vibratePattern)
                .setContentIntent(notificationIntent)
                .build();
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(notificationID, notificationOnStop);
    }

    private void createErrorNotification(long length) {
        if(!errorNotifications) return;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(notificationID+1);
        long[] vibratePattern = {0, 125, 25, 125, 25, 125};
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("A pause occured")
                .setSmallIcon(notificationIcon)
                .setContentText(""+((float) length) / 1000000000L + " : " + delay)
                .setVibrate(vibratePattern)
                .setContentIntent(notificationIntent)
                .build();
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(notificationID + 1, notification);
    }

    private float[] cloneValueArray(float[] array) {
        float[] res = new float[array.length];
        System.arraycopy(array, 0, res, 0, array.length);

        return res;
    }

    private class SensorEventClone {
        public Sensor sensor;
        public float[] values;
        public long timestamp;

        public SensorEventClone(SensorEvent event) {
            sensor = event.sensor;
            values = cloneValueArray(event.values);
            timestamp = event.timestamp;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        sensorEventQueue.add(new SensorEventClone(event));
    }

    private class SaveDataThread extends Thread {

        @Override
        public void run() {
            while(!isInterrupted() || sensorEventQueue.size() > 0) {
                if (!sensorEventQueue.isEmpty()) {
                    SaveData(sensorEventQueue.get(0));
                    sensorEventQueue.remove(0);
                } else {
                    Thread.yield();
                }
            }
        }

        public void SaveData(SensorEventClone event) {
            try {
                Date curDate = new Date(System.currentTimeMillis());
                Sensor sensor = event.sensor;
                long lastMeasure = 0;
                String fileName = "";

                if (event.sensor == acc) {
                    mGravity = event.values;
                } else if (event.sensor == geoMat) {
                    mGeomagnetic = event.values;
                }
                if (mGravity != null && mGeomagnetic != null) {
                    float R[] = new float[9];
                    float I[] = new float[9];
                    boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
                    if (success) {
                        float orientation[] = new float[3];
                        SensorManager.getOrientation(R, orientation);

                        event.values = orientation;
                        lastMeasure = lastOrientation;
                        fileName = orientationFileName;
                        lastOrientation = lastMeasure;
                        //newEvent.values = orientation;
                    }
                }

                if(sensor == linAcc) {
                    lastMeasure = lastLinAcc;
                    fileName = linAccFileName;
                    lastLinAcc = event.timestamp;
                }
                else if(sensor == gyro) {
                    lastMeasure = lastGyro;
                    fileName = gyroFileName;
                    lastGyro = event.timestamp;
                }

                saveDataToFile(fileName, getOutput(event, lastMeasure, curDate), header);
                if(event.timestamp >= lastMeasure+delay && lastMeasure > 0) {
                    createErrorNotification(event.timestamp - lastMeasure);
                    saveErrorData(curDate, event.timestamp, "acc   ", event.timestamp - lastMeasure);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                System.out.println("Exception isnull: " + (event == null));
                System.out.println();
            }
        }

        public String getOutput(SensorEventClone event, long lastMea, Date curDate) {
            float[] values = event.values;

            String newSpeed = ""+speed;
            String lat = "0";
            String lng = "0";

            try {
                lat = ""+lastLocation.getLatitude();
                lng = ""+lastLocation.getLongitude();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            return  "" + curDate.toString() +
                    "," + event.timestamp +
                    "," + identifier +
                    ","+ mode +
                    ","+values[0] +
                    "," + values[1] +
                    "," + values[2] +
                    "," + ((event.timestamp - lastMea) / 1000000L) +
                    "," + newSpeed +
                    "," + powerManager.isScreenOn() +
                    "," + lat +
                    "," + lng + "\n";

        }

        private void saveErrorData(Date curDate, long timestamp, String sensorType, long pauseLength) {
            String errorString = ""+curDate.toString() + "," + timestamp + "," + getIdentifier() + "," + sensorType + "," + pauseLength + "\n";
            saveDataToFile(errorFileName, errorString, errorHeader);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        sensorMan.unregisterListener(this);

        createStoppedNotification();

        try {
            locationManager.removeUpdates(locationListener);
        } catch (SecurityException ignored) {}

        saveDataThread.interrupt();

        wl.release();
    }

    private boolean saveDataToFile(String fileName, String data, String header) {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            MainActivity.getInstance().debugText("Storage not Available");
            return false;
        }

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName+".txt");
        MainActivity.getInstance().debugText("" + file.getPath());

        String text2write = "";
        if(!file.exists()) {
            text2write = header;
        }
        text2write += data;

        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file, true));
            stream.write(text2write.getBytes());
            stream.close();
            MainActivity.getInstance().debugText(text2write);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MainActivity.getInstance().debugText("File Not Found");
        } catch (IOException e) {
            e.printStackTrace();
            MainActivity.getInstance().debugText("IOException");
        }

        return false;
    }

    /**
     * Indicates to the service that the user has pressed the Event Button.
     */
    public void OnUserEvent() {
        if(!isRunning) {
            return;
        }

        String eventOutput = "" + new Date(System.currentTimeMillis()).toString() + ",EVENT," + getIdentifier() + "," + getMode() + ",0,0,0\n";

        saveDataToFile(linAccFileName, eventOutput, header);
        saveDataToFile(gyroFileName, eventOutput, header);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Returns whether or not the service is currently running.
     * @return Returns true if the service is running. Returns false otherwise
     */
    public static boolean isRunning() {
        return isRunning;
    }

    /**
     * Gets the identifier that the service is currently recording with
     * @return The identifier being recorded
     */
    public static String getIdentifier() {
        return identifier;
    }

    /**
     * Returns the mode that the service is currently recording with
     * @return The mode currently being recorded with
     */
    public static String getMode() {
        return mode;
    }
    public static AccService getInstance() { return instance; }
}