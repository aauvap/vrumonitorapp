package dk.aau.create.mbch.test;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements Runnable {

    private static MainActivity instance;

    private Thread t;
    private TextView tv;
    private Handler handler;

    private static int mode = 0;
    private final String[] modeChoices = {"Bike", "Walk", "Other"};
    private String modeString = modeChoices[mode];

    private boolean isToggled = false;
    private AutoStopThread currentThread = null;


    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.test);
        handler = new Handler();
        instance = this;

        ((TextView) findViewById(R.id.button2)).setText(modeString);

        if(AccService.isRunning()) {
            TextView inputView = (TextView) findViewById(R.id.editText);
            inputView.setText(AccService.getIdentifier());
        }

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                final Writer result = new StringWriter();
                final PrintWriter printWriter = new PrintWriter(result);

                e.printStackTrace(printWriter);
                String stackTrace = result.toString();
                printWriter.close();

                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "exceptionLog " + new Date(System.currentTimeMillis()).toString() + ".txt");

                try {
                    BufferedWriter bos = new BufferedWriter(new FileWriter(file));
                    bos.write(stackTrace);
                    bos.flush();
                    bos.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });


    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            handler.post(new Runnable() {
                @Override
                public void run() {
                    ((TextView) findViewById(R.id.status)).setText(AccService.isRunning() ? "Runnning" : "Not Running");
                }
            });
        }
    }
    private class AutoStopThread implements Runnable {

        private Context context;

        @Override
        public void run() {
            if(AccService.isRunning()) {
                stopService(new Intent(context, AccService.class));
                RestartService.active = false;
            }
        }
        public AutoStopThread(Context context) {
            this.context = context;
        }

    }
    public void toggleButton(View view) {
        final Context context = this;
        if(AccService.isRunning()) {
            stopService(new Intent(context, AccService.class));
            RestartService.active = false;
            handler.removeCallbacks(currentThread);
        } else {
            if(!isToggled) {
                isToggled = true;
                final String id = ((TextView)findViewById(R.id.editText)).getText().toString().trim();
                if(id.equals("")) {
                    ((TextView) findViewById(R.id.status)).setText("ID not viable");
                    isToggled = false;
                    return;
                }

                EditText delayText = (EditText) findViewById(R.id.delayField);
                int waitBeforeTurnon = 0;
                try {
                    waitBeforeTurnon = Integer.parseInt(delayText.getText().toString());
                } catch (Exception ignored) {}
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        beginService(id);
                        isToggled = false;
                    }
                }, waitBeforeTurnon);

                EditText durationText = (EditText) findViewById(R.id.durationField);
                int duration = 0;
                try {
                    duration = Integer.parseInt(durationText.getText().toString());
                } catch (Exception ignored) {}
                if(duration > 0) {
                    AutoStopThread autoStopThread = new AutoStopThread(this);
                    currentThread = autoStopThread;
                    handler.postDelayed(autoStopThread, duration + waitBeforeTurnon);
                }
            }
        }
    }

    private void beginService(String id) {
        boolean errorNotifications = ((CheckBox) findViewById(R.id.errorNotifications)).isChecked();
        startService(new Intent(this, AccService.class)
                .putExtra(AccService.EXTRA_ID, id)
                .putExtra(AccService.EXTRA_MODE, modeString)
                .putExtra(AccService.EXTRA_ERRORNOTIFICATION, errorNotifications));

        RestartService.active = true;

        startService(new Intent(this, RestartService.class)
                .putExtra(RestartService.EXTRA_ID, id)
                .putExtra(RestartService.EXTRA_MODE, mode)
                .putExtra(RestartService.EXTRA_ERROR_NOTIFICATION, errorNotifications));

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(250);
    }

    public void eventButton(View view) {
        if(AccService.getInstance() != null) AccService.getInstance().OnUserEvent();
    }

    public void deleteButton(View view) {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == DialogInterface.BUTTON_POSITIVE) {
                    String state = Environment.getExternalStorageState();
                    if (!Environment.MEDIA_MOUNTED.equals(state)) {
                        MainActivity.getInstance().debugText("Storage not Available");
                        return;
                    }
                    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                    String errorText = "";

                    File file = new File(dir, AccService.linAccFileName+".txt");
                    if(!file.delete()) {
                        errorText += "linAcc not deleted" + '\n';
                    }

                    file = new File(dir, AccService.gyroFileName+".txt");
                    if(!file.delete()) {
                        errorText += "gyro not deleted" + '\n';
                    }

                    file = new File(dir, AccService.orientationFileName+".txt");
                    if(!file.delete()) {
                        errorText += "orientation not deleted";
                    }


                    if(errorText.equals("")) {
                        errorText = "Delete Successful";
                    }
                    debugText(errorText);
                }
            }
        };

        new AlertDialog.Builder(this)
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener)
                .show();


    }

    public void debugText(final String text) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                tv.setText(text);
            }
        });
    }

    public void modeButton(View view) {
        mode = (mode+1) % modeChoices.length;
        modeString = modeChoices[mode];

        ((TextView) view).setText(modeString);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        t.interrupt();
    }

    @Override
    protected void onResume() {
        super.onResume();
        t = new Thread(this);
        t.start();
    }
}
