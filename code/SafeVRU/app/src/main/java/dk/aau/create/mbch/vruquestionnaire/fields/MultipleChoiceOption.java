package dk.aau.create.mbch.vruquestionnaire.fields;

/**
 * Created by mbch on 23/08/16, for the InDev VRU PROJECT
 */
public class MultipleChoiceOption {
    private int id;
    private int multiple_choice_field_id;
    private String caption;
    private String picture;
    private String picture_dir;
    private boolean exclusive;
    private boolean allows_move_on;
    private int priority;

    public int getId() {
        return id;
    }

    public int getMultiple_choice_field_id() {
        return multiple_choice_field_id;
    }

    public String getCaption() {
        return caption;
    }

    public String getPicture() {
        return picture;
    }

    public String getPicture_dir() {
        return picture_dir;
    }

    public boolean isExclusive() {
        return exclusive;
    }

    public boolean isAllows_move_on() {
        return allows_move_on;
    }

    public int getPriority() {
        return priority;
    }

    public String getPictureURI() {
        if(picture_dir == "" || picture == "" || picture_dir == null || picture == null) {
            return null;
        } else {
            return "http://vru.create.aau.dk/files/multiplechoiceoptions/picture/"+picture_dir+"/" + picture;
        }
    }
}
