package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import dk.aau.create.mbch.vruquestionnaire.NotificationService;
import dk.aau.create.mbch.vruquestionnaire.QuestionnaireController;
import dk.aau.create.mbch.vruquestionnaire.R;
import dk.aau.create.mbch.vruquestionnaire.answers.Answer;
import dk.aau.create.mbch.vruquestionnaire.answers.DateAnswer;
import dk.aau.create.mbch.vruquestionnaire.answers.FreeTextAnswer;
import dk.aau.create.mbch.vruquestionnaire.answers.MapAnswer;
import dk.aau.create.mbch.vruquestionnaire.answers.MultipleChoiceAnswer;
import dk.aau.create.mbch.vruquestionnaire.answers.NumberAnswer;
import dk.aau.create.mbch.vruquestionnaire.fields.DateField;
import dk.aau.create.mbch.vruquestionnaire.fields.FreeTextField;
import dk.aau.create.mbch.vruquestionnaire.fields.MapField;
import dk.aau.create.mbch.vruquestionnaire.fields.MultipleChoiceField;
import dk.aau.create.mbch.vruquestionnaire.fields.MultipleChoiceOption;
import dk.aau.create.mbch.vruquestionnaire.fields.NumberField;
import dk.aau.create.mbch.vruquestionnaire.fields.QuestionField;

public class Questionnaire extends AppCompatActivity {
    private Handler handler;
    private LinearLayout fieldViewContainer;
    private ScrollView scrollView;
    private TextView questionText;
    private AnswerFieldInterface[] answerFieldInterfaces = null;
    private MapGroup currentMapGroup = null;
    private final int mapRequestID = 123;
    private final ViewGroup.LayoutParams elementParams = new ViewGroup.LayoutParams(-1,-2);
    private QuestionnaireController questionnaireController;
    private boolean questionLoaded = false;
    private View progressBar;
    private boolean firstPanel = false;
    private boolean lastPanel = false;
    private final Context context = this;
    private ArrayList<CompoundButton> allowContinueButtons = new ArrayList<CompoundButton>();

    public static final String EXTRA_NEW = "newParticipant";
    public static final String EXTRA_TEST = "testRun";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();

        ImageLoader.getInstance().init(config);

        questionText = (TextView) findViewById(R.id.questionText);

        fieldViewContainer = (LinearLayout) findViewById(R.id.answerContainer);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        handler = new Handler();

        questionnaireController = new QuestionnaireController(this, getIntent().getBooleanExtra(EXTRA_NEW, false), getIntent().getBooleanExtra(EXTRA_TEST, false));

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Loads a new question to be shown on the screen.
     * @param question The question to be loaded
     */
    public void loadQuestion(final Question question) {
        final Context context = this;
        final int errorTextColor = ContextCompat.getColor(context, R.color.colorErrorText);
        if(question != null) {

            handler.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    resetQuestion();
                    questionText.setText(formatLine(question.getQuestion()));
                    QuestionField[] questionFields = question.getQuestionFields();
                    answerFieldInterfaces = new AnswerFieldInterface[questionFields.length];

                    for (int i = 0; i < questionFields.length; i++) {
                        AnswerFieldInterface answerFieldInterface = null;
                        switch (questionFields[i].getFieldType()) {
                            case FreeText:
                                final FreeTextField freeTextField = (FreeTextField) questionFields[i];

                                addCaption(freeTextField.getCaption());

                                String startText = freeTextField.getSample_text();
                                final EditText editText = addEditField(startText, freeTextField.getMax_length());
                                final int startTextColor = editText.getCurrentTextColor();

                                answerFieldInterface = new AnswerFieldInterface() {

                                    @Override
                                    public void setIsWrong(boolean isWrong) {
                                        if(isWrong) {
                                            editText.setTextColor(errorTextColor);
                                        } else {
                                            editText.setTextColor(startTextColor);
                                        }
                                    }
                                    @Override
                                    public Answer getAnswer() throws InvalidInputException {
                                        String text = editText.getText().toString();
                                        if(!allowMoveOn()) {
                                            if(text.length() < freeTextField.getMin_length()) {
                                                throw new InvalidInputException(getResources().getString(R.string.questionnaire_freeTextError));
                                            } else if(freeTextField.isValidate_as_email() && !text.contains("@")) {
                                                throw new InvalidInputException(getString(R.string.questionnaire_freeTextEmailError));
                                            }
                                        }
                                        return new FreeTextAnswer(freeTextField.getId(), editText.getText().toString());
                                    }
                                };
                                break;
                            case Date:
                                final DateField dateField = (DateField) questionFields[i];
                                addCaption(dateField.getCaption());
                                //tempDateTimeGroup = addDateTimeGroup(dateField);

                                final DateTimeGroup dateTimeGroup = addDateTimeGroup(dateField);
                                answerFieldInterface = new AnswerFieldInterface() {
                                    @Override
                                    public Answer getAnswer() throws InvalidInputException {
                                        return new DateAnswer(context, dateField.getId(), dateTimeGroup.getDate(), dateTimeGroup.isTimeKnown(), dateTimeGroup.isDateKnown());
                                    }

                                    @Override
                                    public void setIsWrong(boolean isWrong) {
                                        if(isWrong) {
                                            dateTimeGroup.highlightProblemButton();
                                        } else {
                                            dateTimeGroup.removeHighlight();
                                        }

                                    }
                                };
                                break;
                            case Number:
                                final NumberField numberField = (NumberField) questionFields[i];
                                addCaption(numberField.getCaption());

                                final EditText numberText = addNumberField(numberField.isAllow_floats());
                                final int startNumberColor = numberText.getCurrentTextColor();

                                final float minNum = numberField.getMin_number();
                                final float maxNum = numberField.getMax_number();
                                final boolean useNumLimits = minNum != 0 || maxNum != 0;
                                answerFieldInterface = new AnswerFieldInterface() {
                                    @Override
                                    public Answer getAnswer() throws InvalidInputException {
                                        String output = numberText.getText().toString();
                                        try {
                                            float num = Float.parseFloat(output);
                                            if ((num < minNum || num > maxNum) && useNumLimits && !allowMoveOn()) {
                                                String message = getString(R.string.questionnaire_numberOutOfBounds, minNum, maxNum);
                                                throw new InvalidInputException(message);
                                            }
                                            float number = Float.parseFloat(numberText.getText().toString());
                                            return new NumberAnswer(numberField.getId(), number);
                                        } catch (NumberFormatException e) {
                                            if(allowMoveOn()) {
                                                return new NumberAnswer(numberField.getId(), 0);
                                            } else {
                                                String message = getString(R.string.questionnaire_numberWrongFormat);
                                                throw new InvalidInputException(message);
                                            }
                                        }
                                    }

                                    @Override
                                    public void setIsWrong(boolean isWrong) {
                                        if(isWrong) {
                                            numberText.setTextColor(errorTextColor);
                                        } else {
                                            numberText.setTextColor(startNumberColor);
                                        }
                                    }
                                };
                                break;

                            case MultipleChoice:
                                final MultipleChoiceField multipleChoiceField = (MultipleChoiceField) questionFields[i];
                                if (multipleChoiceField.getMin_num_choices() == 1 && multipleChoiceField.getMax_num_choices() == 1) {

                                    final Questionnaire.RadioGroup radioGroup = addRadioGroup(multipleChoiceField.getCaption(), multipleChoiceField.getMultiple_choice_options());

                                    answerFieldInterface = new AnswerFieldInterface() {
                                        @Override
                                        public Answer getAnswer() throws InvalidInputException {
                                            int checkedID = radioGroup.getCheckedID();
                                            if (checkedID == -1) {
                                                String message = getResources().getString(R.string.questionnaire_radioButtonNotSelected);
                                                throw new InvalidInputException(message);
                                            }

                                            int[] optionID = {multipleChoiceField.GetOptionIDbyID(checkedID)};
                                            return new MultipleChoiceAnswer(optionID);
                                        }

                                        @Override
                                        public void setIsWrong(boolean isWrong) {
                                            radioGroup.markWrong(isWrong);
                                        }
                                    };
                                } else {
                                    final CheckboxGroup checkboxGroup = addCheckboxGroup(multipleChoiceField.getCaption(), multipleChoiceField.getMultiple_choice_options(), multipleChoiceField.getMin_num_choices(), multipleChoiceField.getMax_num_choices());
                                    answerFieldInterface = new AnswerFieldInterface() {
                                        @Override
                                        public Answer getAnswer() throws InvalidInputException {
                                            if (!checkboxGroup.isSaturated()) {
                                                String message = getResources().getString(R.string.questionnaire_answersOutOfBounds, multipleChoiceField.getMin_num_choices(), multipleChoiceField.getMax_num_choices());
                                                throw new InvalidInputException(message);
                                            }

                                            ArrayList<Integer> checkList = new ArrayList<>();
                                            for (int i = 0; i < checkboxGroup.checkBoxes.length; i++) {
                                                if (checkboxGroup.checkBoxes[i].isChecked()) {
                                                    checkList.add(multipleChoiceField.GetOptionIDbyID(i));
                                                }
                                            }
                                            int[] resultArray = new int[checkList.size()];
                                            for (int i = 0; i < resultArray.length; i++) {
                                                resultArray[i] = checkList.get(i);
                                            }

                                            return new MultipleChoiceAnswer(resultArray);
                                        }

                                        @Override
                                        public void setIsWrong(boolean isWrong) {
                                            checkboxGroup.markWrong(isWrong);
                                        }
                                    };
                                }
                                break;
                            case Map:
                                final MapField mapField = (MapField) questionFields[i];
                                double[] longs = new double[0];
                                double[] lats = new double[0];

                                final MapGroup mapGroup = addMapGroup(mapField.getCaption(), mapField.getMin_num_marks(), mapField.getMax_num_marks(), lats, longs);
                                answerFieldInterface = new AnswerFieldInterface() {
                                    @Override
                                    public Answer getAnswer() throws InvalidInputException {
                                        if(!allowMoveOn()) {
                                            if(mapGroup.getLatitudes().length < mapField.getMin_num_marks() || mapGroup.getLatitudes().length > mapField.getMax_num_marks()) {
                                                throw new InvalidInputException(getString(R.string.questionnaire_mapError));
                                            }
                                        }
                                        return new MapAnswer(mapField.getId(), mapGroup.getLatitudes(), mapGroup.getLongitudes());
                                    }

                                    @Override
                                    public void setIsWrong(boolean isWrong) {

                                    }
                                };
                                break;
                        }

                        answerFieldInterfaces[i] = answerFieldInterface;
                    }
                    questionLoaded = true;
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        } else {
            CreateToast("Question could not be found");
        }
    }

    public void loadStartScreen(final String text) {
        final Context context = this;//TODO Perhaps add title also
        handler.post(new Runnable() {
            @Override
            public void run() {
                resetQuestion();
                questionText.setVisibility(View.GONE);
                TextView textView = (TextView) getLayoutInflater().inflate(R.layout.template_large_text, fieldViewContainer).findViewById(R.id.textView);
                textView.setText(formatLine(text));
                firstPanel = true;
                questionLoaded = true;
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void loadEndScreen(final String text) {
        final Context context = this;
        handler.post(new Runnable() {
            @Override
            public void run() {
                resetQuestion();
                questionText.setVisibility(View.GONE);
                TextView textView = (TextView) getLayoutInflater().inflate(R.layout.template_large_text, fieldViewContainer).findViewById(R.id.textView);
                textView.setText(formatLine(text));
                lastPanel = true;
                progressBar.setVisibility(View.GONE);
                questionLoaded = true;
                Button button = (Button) findViewById(R.id.continueButton);
                button.setText(getString(R.string.questionnaire_finish));
                findViewById(R.id.backButton).setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == mapRequestID && resultCode == RESULT_OK && currentMapGroup != null) {
            double[] latExtra = data.getDoubleArrayExtra("latitudes");
            double[] longExtra = data.getDoubleArrayExtra("longitudes");
            currentMapGroup.setLatLng(latExtra, longExtra);
            String buttonText = getString(R.string.questionnaire_locationButton, latExtra.length);
            currentMapGroup.getButton().setText(buttonText);
            currentMapGroup = null;
        }
    }

    public class InvalidInputException extends Exception {
        public String message;
        public InvalidInputException(String message) {
            super(message);
            this.message = message;
        }
    }

    public void onBackButton(View view) {
        if(firstPanel || lastPanel) {
            finish();
        } else {
            resetQuestion();
            questionnaireController.loadPreviousQuestion(this);
        }
    }

    /**
     * Called when the Continue Button is pushed
     * @param view A reference to the continue button
     */
    public void onContinueButton(View view) {
        if(questionLoaded) {
            if(lastPanel) {
                finish();
            } else if(firstPanel) {
                questionnaireController.loadCurrentQuestion(this);
            } else {

                Answer[] answers = new Answer[answerFieldInterfaces.length];

                for(int i = 0; i < answerFieldInterfaces.length; i++) {
                    AnswerFieldInterface current = answerFieldInterfaces[i];
                    System.out.println("Checking: " + i);
                    try {
                        answers[i] = current.getAnswer();
                        System.out.println("After setIsWrong(false)");
                        current.setIsWrong(false);
                    } catch (InvalidInputException e) {
                        CreateToast(e.message);
                        System.out.println("before setIsWrong(true)");
                        current.setIsWrong(true);

                        for(i += 1;i < answerFieldInterfaces.length; i++) {
                            answerFieldInterfaces[i].setIsWrong(false);
                        }

                        return;
                    }
                }

                questionnaireController.receiveAnswer(answers, this);
                resetQuestion();

            }

        }

    }

    public void onBackPressed() {
        onBackButton(null);
    }

    public static class TimePickerFragment extends DialogFragment {
        TimePickerDialog.OnTimeSetListener listener;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            Bundle bundle = getArguments();
            int
                minute,
                hour;

            if(bundle.getBoolean("used")) {
                minute = bundle.getInt("minute");
                hour = bundle.getInt("hour");
            } else {
                minute = c.get(Calendar.MINUTE);
                hour = c.get(Calendar.HOUR_OF_DAY);
            }

            return new TimePickerDialog(getActivity(), listener, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }

        public TimePickerFragment addListener(TimePickerDialog.OnTimeSetListener listener) {
            this.listener = listener;
            return this;
        }
    }

    public static class DatePickerFragment extends DialogFragment {
        DatePickerDialog.OnDateSetListener listener;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar c = Calendar.getInstance();
            Bundle bundle = getArguments();
            int
                year,
                month,
                day;

            if(bundle.getBoolean("used")) {
                year = bundle.getInt("year");
                month = bundle.getInt("month");
                day = bundle.getInt("day");
            } else {
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            }

            return new DatePickerDialog(getActivity(), listener, year, month, day);
        }

        public DatePickerFragment addListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
            return this;
        }

    }

    /**
     * Resets the screen, so that a new question can be loaded. Called by loadQuestion()
     */
    private void resetQuestion() {
        questionText.setText("");
        questionText.setVisibility(View.VISIBLE);
        scrollView.fullScroll(View.FOCUS_UP);
        fieldViewContainer.removeAllViews();
        questionLoaded = false;
        answerFieldInterfaces = new AnswerFieldInterface[0];
        findViewById(R.id.backButton).setEnabled(true);
        ((Button) findViewById(R.id.continueButton)).setText(getString(R.string.questionnaire_continue));
        firstPanel = false;
        lastPanel = false;
        allowContinueButtons.clear();
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * An interface for getting the results of the different answer fields.
     */
    private interface AnswerFieldInterface {
        Answer getAnswer() throws InvalidInputException;
        void setIsWrong(boolean isWrong);
    }

    /**
     * Adds an EditText view to the screen.
     * @param sample The text to be displayed in the EditText field
     * @return The EditText view created.
     */
    private EditText addEditField(String sample, int maxLength) {
        EditText editText = new EditText(this);
        editText.setLayoutParams(elementParams);
        editText.setHint(sample);
        if(maxLength > 0) {
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        }
        fieldViewContainer.addView(editText);

        return editText;
    }

    private EditText addNumberField(boolean isFloat) {
        EditText editText = new EditText(this);
        editText.setLayoutParams(elementParams);
        editText.setText("");
        int inputType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED;
        if(isFloat) {
            inputType = inputType | InputType.TYPE_NUMBER_FLAG_DECIMAL;
        }
        editText.setInputType(inputType);
        fieldViewContainer.addView(editText);

        return editText;
    }

    /**
     * Ads a non-interactable caption field to the screen.
     * @param caption The text to be displayed in the caption.
     */
    private void addCaption(String caption) {
        TextView tv = new TextView(this);
        tv.setLayoutParams(elementParams);
        tv.setText(formatLine(caption));
        tv.setPadding(0, 50, 0, 0);
        fieldViewContainer.addView(tv);
    }

    private class RadioGroup implements CompoundButton.OnCheckedChangeListener {

        private ArrayList<RadioButton> radioButtons = new ArrayList<>();
        private int checkedID = -1;
        private int errorTextColor = -1;
        private int defaultTextColor = -1;

        public void addRadioButton(RadioButton radioButton) {
            radioButtons.add(radioButton);
            radioButton.setOnCheckedChangeListener(this);
            radioButton.setId(View.generateViewId());
            if(defaultTextColor == -1) {
                defaultTextColor = radioButton.getCurrentTextColor();
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                for (int i = 0; i < radioButtons.size(); i++) {
                    RadioButton radioButton = radioButtons.get(i);
                    if (radioButton != buttonView) {
                        radioButton.setChecked(false);
                    } else {
                        checkedID = i;
                    }
                }
            }
        }

        public int getCheckedID() {
            return checkedID;
        }

        public void markWrong(boolean isWrong) {
            if(errorTextColor == -1) {
                errorTextColor = ContextCompat.getColor(context, R.color.colorErrorText);
            }
            for(RadioButton rb : radioButtons) {
                rb.setTextColor(isWrong ? errorTextColor : defaultTextColor);
            }
        }
    }

    private boolean allowMoveOn() {
        for(CompoundButton cb : allowContinueButtons) {
            if(cb.isChecked()) {
                return true;
            }
        }
        return false;
    }

    private RadioGroup addRadioGroup(String title, MultipleChoiceOption[] options) {
        addCaption(title);

        int imgSize = fieldViewContainer.getWidth()/2;
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(imgSize, imgSize);

        RadioGroup radioGroup = new RadioGroup();

        ImageLoader imageLoader = ImageLoader.getInstance();

        for (int i = 0; i < options.length; i++) {
            if(options[i].getPictureURI() != null) {
                //Image Fields

                //Inflate layout
                final LinearLayout element = (LinearLayout) getLayoutInflater().inflate(R.layout.radiobutton_image_field, null);
                element.findViewById(R.id.frame).setLayoutParams(imageParams);

                //Set Text
                ((TextView) element.findViewById(R.id.text)).setText(formatLine(options[i].getCaption()));

                //Set Image
                ImageView imageView = (ImageView) element.findViewById(R.id.image);
                imageLoader.displayImage(options[i].getPictureURI(), imageView);

                //Make Sure image is clickable
                final RadioButton radioButton = (RadioButton) element.findViewById(R.id.radiobutton);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        radioButton.performClick();
                    }
                });

                if(options[i].isAllows_move_on()){
                    allowContinueButtons.add(radioButton);
                }

                //Add radioButton to the radiogroup
                radioGroup.addRadioButton(radioButton);

                //Hide Keyboard when clicked
                radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboard();
                    }
                });

                //Add element to the scene
                fieldViewContainer.addView(element);

            } else {
                //Non-image fields

                //Inflate layout
                LinearLayout element = (LinearLayout) getLayoutInflater().inflate(R.layout.radiobutton_nonimage_field, null);

                //Set Text
                RadioButton radioButton = (RadioButton) element.findViewById(R.id.radiobutton);
                radioButton.setText(formatLine(options[i].getCaption()));

                if(options[i].isAllows_move_on()){
                    allowContinueButtons.add(radioButton);
                }

                //Add radiobutton to the radiogroup
                radioGroup.addRadioButton((RadioButton) element.findViewById(R.id.radiobutton));

                //Hide Keyboard when clicked
                radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboard();
                    }
                });

                //Add element to the scene
                fieldViewContainer.addView(element);
            }
        }

        return radioGroup;
    }

    /**
     * Adds a group of checkbox fields to the screen.
     * @param title The title of the group, to be shown above the checkboxes.
     * @param options The options up for choice.
     * @param minAnswer The minimum number of checkboxes that must be checked by the user.
     * @param maxAnswer The maximum number of checkboxes that can be checked by the user.
     * @return The CheckboxGroup created.
     */
    private CheckboxGroup addCheckboxGroup(String title, final MultipleChoiceOption[] options, final int minAnswer, final int maxAnswer) {
        addCaption(title);
        int imgSize = fieldViewContainer.getWidth()/2;
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(imgSize, imgSize);

        final CheckboxGroup checkboxGroup = new CheckboxGroup(options.length, minAnswer, maxAnswer);
        ImageLoader imageLoader = ImageLoader.getInstance();

        for(int i = 0; i < options.length; i++) {
            final boolean exclusive = options[i].isExclusive();
            if(options[i].getPictureURI() != null) {
                //ImageFields

                //Inflate Layout
                LinearLayout checkboxField = (LinearLayout) getLayoutInflater().inflate(R.layout.checkbox_image_field, null);
                checkboxField.findViewById(R.id.frame).setLayoutParams(imageParams);

                //Add checkbox to group
                final CheckBox checkBox = (CheckBox) checkboxField.findViewById(R.id.checkbox);
                checkboxGroup.checkBoxes[i] = checkBox;

                if(options[i].isAllows_move_on()){
                    allowContinueButtons.add(checkBox);
                }

                //Create Max Checking and Exclusive Checking
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked && checkboxGroup.isFilledUp()) {
                            String message = getString(R.string.questionnaire_answersOutOfBounds, minAnswer, maxAnswer);
                            CreateToast(message);
                            checkBox.setChecked(false);
                        } else if (isChecked) {
                            if(exclusive) {
                                for(CheckBox c : checkboxGroup.checkBoxes) {
                                    if(c != checkBox) {
                                        c.setChecked(false);
                                    }
                                }
                            } else {
                                for(int i = 0; i < options.length; i++) {
                                    CheckBox c = checkboxGroup.checkBoxes[i];
                                    if(c != checkBox && options[i].isExclusive()){
                                        c.setChecked(false);
                                    }
                                }
                            }
                        }
                    }
                });

                //Set Text
                ((TextView) checkboxField.findViewById(R.id.text)).setText(formatLine(options[i].getCaption()));

                //Set Image
                ImageView imageView = (ImageView) checkboxField.findViewById(R.id.image);
                imageLoader.displayImage(options[i].getPictureURI(), imageView);

                //Make sure the image is clickable
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkBox.performClick();
                    }
                });

                //Hide Keyboard when clicked
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboard();
                    }
                });

                fieldViewContainer.addView(checkboxField);

            } else {
                //Non-ImageFields

                //Inflate Layout
                LinearLayout checkboxField = (LinearLayout) getLayoutInflater().inflate(R.layout.checkbox_nonimage_field, null);

                //Add checkbox to group
                final CheckBox checkBox = (CheckBox) checkboxField.findViewById(R.id.checkbox);
                checkboxGroup.checkBoxes[i] = checkBox;

                if(options[i].isAllows_move_on()){
                    allowContinueButtons.add(checkBox);
                }

                //Create Max Checking and Exclusive Checking
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked && checkboxGroup.isFilledUp()) {
                            String message = getString(R.string.questionnaire_answersOutOfBounds, minAnswer, maxAnswer);
                            CreateToast(message);
                            checkBox.setChecked(false);
                        } else if (isChecked) {
                            if (exclusive) {
                                for (CheckBox c : checkboxGroup.checkBoxes) {
                                    if (c != checkBox) {
                                        c.setChecked(false);
                                    }
                                }
                            } else {
                                for (int i = 0; i < options.length; i++) {
                                    CheckBox c = checkboxGroup.checkBoxes[i];
                                    if (c != checkBox && options[i].isExclusive()) {
                                        c.setChecked(false);
                                    }
                                }
                            }
                        }
                    }
                });

                //Set Text
                checkBox.setText(options[i].getCaption());

                //Hide Keyboard when clicked
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeyboard();
                    }
                });

                fieldViewContainer.addView(checkboxField);
            }
        }

        return checkboxGroup;
    }

    private DateTimeGroup addDateTimeGroup(final DateField dateField) {
        View view = getLayoutInflater().inflate(R.layout.date_field, fieldViewContainer);
        Button dateButton = (Button) view.findViewById(R.id.dateButton);
        final TextView dateText = (TextView) view.findViewById(R.id.dateText);
        final DateTimeGroup dateTimeGroup = new DateTimeGroup(dateField);
        dateTimeGroup.setDateButton(dateButton);
        final boolean hasDontKnow = true; //TODO hack. Remove when database implementation is done.
        final CheckBox dateEscape = (CheckBox) view.findViewById(R.id.dateEscape);
        final CheckBox timeEscape = (CheckBox) view.findViewById(R.id.timeEscape);

        if(!hasDontKnow) {
            view.findViewById(R.id.dateEscapeGroup).setVisibility(View.GONE);
            view.findViewById(R.id.timeEscapeGroup).setVisibility(View.GONE);
        } else {
            dateEscape.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    dateTimeGroup.setDate(!isChecked);
                    if(isChecked) {
                        dateText.setText("");
                    } else {
                        if(dateTimeGroup.datePicked) {
                            Calendar cal = dateTimeGroup.getCalendar();
                            String formatted = String.format("%04d", cal.get(Calendar.YEAR)) + "-" + String.format("%02d", cal.get(Calendar.MONTH)+1) + "-" + String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
                            dateText.setText(formatted);
                        } else {
                            dateText.setText(getString(R.string.questionnaire_pickDate));
                        }
                    }
                }
            });
        }

        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePickerFragment = new DatePickerFragment()
                        .addListener(new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, month);
                                calendar.set(Calendar.DAY_OF_MONTH, day);
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);

                                Date setDate = calendar.getTime();
                                try {
                                    boolean continueFlag = true;
                                    if(dateField.getMin_date() != null) {
                                        if(setDate.getTime() < dateField.getMin_date().getTime()) {
                                            CreateToast(getString(R.string.questionnaire_dateTooEarly));
                                            continueFlag = false;
                                        }
                                    }
                                    if(dateField.getMax_date() != null) {
                                        if (setDate.getTime() > dateField.getMax_date().getTime()) {
                                            CreateToast(getString(R.string.questionnaire_dateTooLate));
                                            continueFlag = false;
                                        }
                                    }
                                    if(continueFlag) {
                                        dateTimeGroup.setDate(year, month, day);
                                        String formatted = String.format("%04d", year) + "-" + String.format("%02d", month+1) + "-" + String.format("%02d", day);
                                        dateText.setText(formatted);
                                    }

                                } catch (ParseException e) {
                                    dateTimeGroup.setDate(year, month, day);
                                    String formatted = String.format("%04d", year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);
                                    dateText.setText(formatted);
                                }
                                dateEscape.setChecked(false);
                            }
                        });
                Bundle args = new Bundle();
                datePickerFragment.setArguments(args);
                datePickerFragment.show(getFragmentManager(), "datePicker");
            }
        });

        if(dateField.isAllow_time()) {
            Button timeButton = (Button) view.findViewById(R.id.timeButton);
            dateTimeGroup.setTimeButton(timeButton);
            final TextView timeText = (TextView) view.findViewById(R.id.timeText);

            timeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment newFragment = new TimePickerFragment()
                            .addListener(new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    dateTimeGroup.setTime(hourOfDay, minute);
                                    String formatted = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);
                                    timeText.setText(formatted);
                                    timeEscape.setChecked(false);
                                }
                            });


                    Bundle args = new Bundle();

                    newFragment.setArguments(args);

                    newFragment.show(getFragmentManager(), "timePicker");
                }
            });
            if(hasDontKnow) {
                timeEscape.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        dateTimeGroup.setTime(!isChecked);
                        if (isChecked) {
                            timeText.setText("");
                        } else{
                            if(dateTimeGroup.timePicked) {
                                Calendar cal = dateTimeGroup.getCalendar();
                                String formatted = String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", cal.get(Calendar.MINUTE));
                                timeText.setText(formatted);
                            } else {
                                timeText.setText(getString(R.string.questionnaire_pickTime));
                            }
                        }
                    }
                });
            }
        } else {
            view.findViewById(R.id.timeLayout).setVisibility(View.GONE);
        }
        return dateTimeGroup;
    }

    public void setText(final String text) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                addCaption(text);
            }
        });
    }

    private MapGroup addMapGroup(String title, final int min, final int max, double[] lats, double[] longs) {
        Button button = new Button(this);
        final MapGroup mapGroup = new MapGroup(lats, longs, button);
        final Context context = this;

        addCaption(title);

        button.setLayoutParams(elementParams);
        button.setText(getString(R.string.questionnaire_choosePosition));
        fieldViewContainer.addView(button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMapGroup = mapGroup;
                Intent intent = new Intent(context, PickLocationActivity.class);
                try {
                    intent = intent
                            .putExtra("Lat", mapGroup.getLatitudes())
                            .putExtra("Lng", mapGroup.getLongitudes());

                } catch (InvalidInputException e) {
                    e.printStackTrace();
                }
                intent = intent
                        .putExtra("minPoints", min)
                        .putExtra("maxPoints", max);
                startActivityForResult(intent, mapRequestID);
            }
        });

        return mapGroup;
    }

    private class MapGroup {
        private double[] latitudes;
        private double[] longitudes;
        private Button button;

        public MapGroup(double[] latitudes, double[] longitudes, Button button) {
            this.latitudes = latitudes;
            this.longitudes = longitudes;
            this.button = button;
        }

        public void setLatLng(double[] latitudes, double[] longitudes) {
            this.latitudes = latitudes;
            this.longitudes = longitudes;
        }

        public double[] getLongitudes() throws InvalidInputException {
            return longitudes;
        }

        public double[] getLatitudes() throws InvalidInputException {
            return latitudes;
        }

        public Button getButton() {
            return button;
        }
    }

    private class DateTimeGroup {
        private boolean hasTime;
        private boolean datePicked, timePicked;
        private boolean dateKnown = true, timeKnown = true;
        private Calendar calendar = Calendar.getInstance();
        private Button dateButton;
        private Button timeButton;
        private Button problemButton;
        private int errorTextColor;
        private int defaultDateColor;
        private int defaultTimeColor;

        public DateTimeGroup(DateField dateField) {
            this.hasTime = dateField.isAllow_time();
            errorTextColor = ContextCompat.getColor(context, R.color.colorErrorText);
        }

        public void setTime(boolean known) {
            timeKnown = known;
        }

        public void setTime(int hour, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            timePicked = true;
        }

        public void setDate(boolean known) {
            dateKnown = known;
        }

        public void setDate(int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            datePicked = true;
        }

        public Calendar getCalendar() {
            return calendar;
        }

        public Date getDate() throws InvalidInputException {
            if(!datePicked && dateKnown) {
                String message = getString(R.string.questionnaire_dateNotSet);
                problemButton = dateButton;
                throw new InvalidInputException(message);
            }
            if(hasTime && !timePicked && timeKnown) {
                String message = getString(R.string.questionnaire_timeNotSet);
                problemButton = timeButton;
                throw new InvalidInputException(message);
            }
            return new Date(calendar.getTimeInMillis());
        }

        public boolean isDateKnown() {
            return dateKnown;
        }

        public boolean isTimeKnown() {
            return timeKnown;
        }

        public DateTimeGroup setDateButton(Button dateButton) {
            this.dateButton = dateButton;
            defaultDateColor = dateButton.getCurrentTextColor();
            if(problemButton == null) {
                problemButton = dateButton;
            }
            return this;
        }

        public DateTimeGroup setTimeButton(Button timeButton) {
            this.timeButton = timeButton;
            defaultTimeColor = timeButton.getCurrentTextColor();
            return this;
        }

        public void highlightProblemButton() {
            if(dateButton != null) {
                dateButton.setTextColor(dateButton == problemButton ? errorTextColor : defaultDateColor);
            }

            if(timeButton != null) {
                timeButton.setTextColor(timeButton == problemButton ? errorTextColor : defaultTimeColor);
            }
        }

        public void removeHighlight() {
            if(dateButton != null) {
                dateButton.setTextColor(defaultDateColor);
            }
            if(timeButton != null) {
                timeButton.setTextColor(defaultTimeColor);
            }
        }
    }

    private class CheckboxGroup {
        private CheckBox[] checkBoxes;
        private int minAnswer, maxAnswer;
        private int errorTextColor = -1;
        private int defaultTextColor = -1;

        public CheckboxGroup(int length, int minAnswer, int maxAnswer) {
            checkBoxes = new CheckBox[length];
            this.minAnswer = minAnswer;
            this.maxAnswer = maxAnswer;
            errorTextColor = ContextCompat.getColor(context, R.color.colorErrorText);
        }

        public boolean isFilledUp() {
            int res = 0;
            for (CheckBox checkBox : checkBoxes) {
                if (checkBox == null) continue;
                if (checkBox.isChecked()) res++;
            }
            return res > maxAnswer;
        }

        public boolean isSaturated() {
            int res = 0;
            for (CheckBox checkBox : checkBoxes) {
                if (checkBox == null) continue;
                if (checkBox.isChecked()) res++;
                if (res >= minAnswer) return true;
            }

            return false;
        }

        public void markWrong(boolean isWrong) {
            if(defaultTextColor == -1) {
                for(CheckBox checkBox : checkBoxes) {
                    if(checkBox != null) {
                        defaultTextColor = checkBox.getCurrentTextColor();
                        break;
                    }
                }
            }
            for(CheckBox checkBox : checkBoxes) {
                if(checkBox == null) continue;
                checkBox.setTextColor(isWrong ? errorTextColor : defaultTextColor);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_questionnaire, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case R.id.continueAction:
                onContinueButton(null);
                break;
        }
        return false;
    }

    /**
     * Shows a toast message to the screen. Great for debugging purposes.
     * @param toastText The text to be shown in the toast.
     */
    public void CreateToast(final String toastText) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, toastText, duration);
                toast.show();
            }
        });
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class FormatPair {
        public String before;
        public String after;
        public String end = null;

        public FormatPair(String before, String after) {
            this.before = before;
            this.after = after;
        }

        public FormatPair(String before, String after, String end) {
            this.before = before;
            this.after = after;
            this.end = end;
        }
    }

    private final FormatPair[] formatPairs = {
            new FormatPair("**", "<b>", "</b>"),
            new FormatPair("<br />", "<br /><br />"),
            new FormatPair("*", "<i>", "</i>"),
            new FormatPair("__", "<b>", "</b>"),
            new FormatPair("_", "<i>", "</i>"),
            new FormatPair("~~", "<strike>", "</strike>")
    };

    public Spanned formatLine(String line) {
        for (FormatPair fp: formatPairs) {

            if(fp.end == null) {
                line = line.replace(fp.before, fp.after);
            } else {
                String beforeLiteral = Pattern.quote(fp.before);
                String regex = beforeLiteral + "(.*?)" + beforeLiteral;
                line = line.replaceAll(regex, fp.after + "$1" + fp.end);
            }
        }

        System.out.println("Line is: " + line);
        return Html.fromHtml(line);
    }
}
