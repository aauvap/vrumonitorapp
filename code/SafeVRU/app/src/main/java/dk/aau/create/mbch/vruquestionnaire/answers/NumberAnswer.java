package dk.aau.create.mbch.vruquestionnaire.answers;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by mbch on 23/10/15.
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class NumberAnswer extends Answer {
    private int number_field_id;
    private float number;

    public NumberAnswer(int number_field_id, float number) {
        super(FieldType.Number);
        this.number_field_id = number_field_id;
        this.number = number;
    }

    public float getNumber() {
        return number;
    }

}
