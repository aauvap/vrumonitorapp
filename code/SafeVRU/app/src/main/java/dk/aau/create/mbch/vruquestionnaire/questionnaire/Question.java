package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import java.util.ArrayList;
import java.util.Collections;

import dk.aau.create.mbch.vruquestionnaire.fields.*;

/**
 * Created by Mads on 15-09-2015.
 */

@SuppressWarnings("unused")
public class Question {
    private int id;
    private int questionnaire_id;
    private String question;
    private String created;
    private String modified;
    private FreeTextField[] free_text_fields;
    private MapField[] map_fields;
    private DateField[] date_fields;
    private MultipleChoiceField[] multiple_choice_fields;
    private NumberField[] number_fields;
    private Questionnaire questionnaire;
    private transient QuestionField[] questionFields = null;

    public QuestionField[] getQuestionFields() {
        if(questionFields == null) {
            ArrayList<QuestionField> tempList = new ArrayList<>();
            Collections.addAll(tempList, free_text_fields);
            Collections.addAll(tempList, map_fields);
            Collections.addAll(tempList, date_fields);
            Collections.addAll(tempList, multiple_choice_fields);
            Collections.addAll(tempList, number_fields);

            questionFields = tempList.toArray(new QuestionField[tempList.size()]);

            boolean sorted = false;
            while(!sorted) {
                sorted = true;
                for(int i = 1; i < questionFields.length; i++) {
                    if(questionFields[i-1].getPriority() > questionFields[i].getPriority()) {
                        QuestionField temp = questionFields[i-1];
                        questionFields[i-1] = questionFields[i];
                        questionFields[i] = temp;
                        sorted = false;
                    }
                }
            }
        }

        return questionFields;
    }

    private class Questionnaire {
        public String intro_text;
        public String end_text;
    }


    public int getId() {
        return id;
    }

    public int getQuestionnaire_id() {
        return questionnaire_id;
    }

    public String getQuestion() {
        return question;
    }

    public String getCreated() {
        return created;
    }

    public String getModified() {
        return modified;
    }

    public FreeTextField[] getFree_text_fields() {
        return free_text_fields;
    }

    public MapField[] getMap_fields() {
        return map_fields;
    }

    public DateField[] getDate_fields() {
        return date_fields;
    }

    public MultipleChoiceField[] getMultiple_choice_fields() {
        return multiple_choice_fields;
    }

    public NumberField[] getNumber_fields() {
        return number_fields;
    }

    public String getIntroText() {
        return questionnaire.intro_text;
    }

    public String getEndText() {
        return questionnaire.end_text;
    }
}