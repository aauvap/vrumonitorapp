package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.content.Context;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by mbch on 11/10/16, for the InDev VRU PROJECT
 */
public class NotificationArchive {
    private static String destination = "notificationArchive2";
    public static class Item {
        public MonthItem[] months;
    }
    private static class MonthItem {
        public int monthCode;
        public int remindersMade = 1;
        public boolean monthActive = true;
    }

    public static void modifyMonth(final Context context, boolean deactivateMonth) {
        for(int k = 0; k < 10; k++) {
            try {
                Item item = loadItem(context);
                int currentMonth = getMonth();
                if(item != null) {
                    ArrayList<MonthItem> monthList = new ArrayList<>();
                    MonthItem newItem = null;
                    for (MonthItem i : item.months) {
                        monthList.add(i);
                        if(i.monthCode == currentMonth) {
                            newItem = i;
                            if(deactivateMonth) {
                                i.monthActive = false;
                            } else {
                                i.remindersMade += 1;
                                if(i.remindersMade > 3) {
                                    i.monthActive = false;
                                }
                            }
                        }
                    }
                    if(newItem == null) {
                        newItem = new MonthItem();
                        newItem.monthCode = getMonth();
                        monthList.add(newItem);
                    }
                    item.months = new MonthItem[monthList.size()];
                    item.months = monthList.toArray(item.months);
                } else {
                    item = new Item();
                    MonthItem newItem = new MonthItem();
                    newItem.monthCode = getMonth();
                    item.months = new MonthItem[] {newItem};
                }
                saveItem(context, item);
                k = 10;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean monthActive(final Context context) throws IOException {
        Item item = loadItem(context);
        System.out.println("item isnull: " + (item == null));
        if(item != null) {
            int month = getMonth();
            for (MonthItem i : item.months) {
                if(i.monthCode == month) {
                    return i.monthActive;
                }
            }
        }
        System.out.println("Returning False");
        return true;
    }

    public static boolean deleteArchive(Context context) {
        File file = new File(context.getFilesDir(), destination);
        return file.delete();
    }

    public static String getText(Context context) {
        try {
            if(new File(context.getFilesDir(), destination).exists()) {
                FileInputStream stream = context.openFileInput(destination);
                byte[] buffer = new byte[stream.available()];
                stream.read(buffer);
                stream.close();
                return new String(buffer);
            } else {
                return "File Dont Exist";
            }
        } catch (IOException e) {
            return "Exception";
        }
    }

    private static int getMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 12 * (calendar.get(Calendar.YEAR) - 2016);
    }

    private static Item loadItem(Context context) throws IOException {
        if(new File(context.getFilesDir(), destination).exists()) {
            FileInputStream stream = context.openFileInput(destination);
            Gson gson = new Gson();
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            String string = new String(buffer);
            System.out.println("Item Loaded: " + string);
            return gson.fromJson(string, Item.class);
        } else {
            return null;
        }
    }

    private static void saveItem(Context context, Item item) throws IOException {
        FileOutputStream stream = context.openFileOutput(destination, Context.MODE_PRIVATE);
        String gson = new Gson().toJson(item);
        stream.write(gson.getBytes());
        stream.close();
    }
}
