package dk.aau.create.mbch.vruquestionnaire;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mbch on 26/04/16, for the InDev VRU PROJECT
 */
public class AppSettingsHandler {

    private static String appDataCachePath = "appDataCache";

    public static AppSettings getAppSettings(Context context) throws IOException {
        String appSettingsURL = "https://vru.create.aau.dk/app_settings.json";
        WebInterfaceConnection webInterfaceConnection = new WebInterfaceConnection(context);
        Gson gson = new Gson();
        String output;
        AppSettings res;
        boolean jsonPassed = false;

        output = gson.fromJson(webInterfaceConnection.getTextFromURL(appSettingsURL), SettingsOutput.class).appSettings;
        System.out.println("appSettings: " + output);


        try {
            res = gson.fromJson(output, AppSettings.class);
            jsonPassed = true;
        } catch (JsonSyntaxException e) {
            System.out.println("Json Fallback");
            String loadedAppData = loadAppData(context);
            if(loadedAppData != null) {
                res = gson.fromJson(loadedAppData, AppSettings.class);
            } else {
                throw new IOException("No Connection");
            }
        }

        if(jsonPassed) {
            cacheAppData(context, output);
        }

        return res;
    }

    private static void cacheAppData(Context context, String appData) {
        try {
            FileOutputStream stream = context.openFileOutput(appDataCachePath, Context.MODE_PRIVATE);
            stream.write(appData.getBytes());
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String loadAppData(Context context) {
        File file = new File (context.getFilesDir(), appDataCachePath);
        if(file.exists()) {
            try {
                FileInputStream stream = context.openFileInput(appDataCachePath);
                byte[] buffer = new byte[stream.available()];
                stream.read(buffer);
                return new String(buffer);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private class SettingsOutput {
        public String appSettings;


    }

}
