package dk.aau.create.mbch.vruquestionnaire.fields;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by Mads on 15-10-2015.
 */
@SuppressWarnings("unused")
public class MultipleChoiceField extends QuestionField {
    private int min_num_choices;
    private int max_num_choices;
    private MultipleChoiceOption[] multiple_choice_options;
    private transient String[] allCaptions = null;
    private transient String[] allImagePaths = null;

    public int getMin_num_choices() {
        return min_num_choices;
    }

    public int getMax_num_choices() {
        return max_num_choices;
    }

    public MultipleChoiceOption[] getMultiple_choice_options() {
        return multiple_choice_options;
    }

    public String[] getAllCaptions() {
        if(allCaptions == null) {
            allCaptions = new String[multiple_choice_options.length];
            for(int i = 0; i < allCaptions.length; i++) {
                allCaptions[i] = multiple_choice_options[i].getCaption();
            }
        }

        return allCaptions;
    }

    public String[] getAllImagePaths() {
        if(allImagePaths == null) {
            allImagePaths = new String[multiple_choice_options.length];
            for(int i = 0; i < allImagePaths.length; i++) {
                allImagePaths[i] = multiple_choice_options[i].getPictureURI();
            }
        }
        return allImagePaths;
    }

    public int GetOptionIDbyCaption(String caption) throws UnknownIDException {
        for (MultipleChoiceOption o : multiple_choice_options) {
            if(o.getCaption().equals(caption)) {
                return o.getId();
            }
        }
        throw new UnknownIDException();
    }

    public int GetOptionIDbyID (int id) {
        return multiple_choice_options[id].getId();
    }

    public class UnknownIDException extends Exception {}



    @Override
    public FieldType getFieldType() {
        return FieldType.MultipleChoice;
    }
}