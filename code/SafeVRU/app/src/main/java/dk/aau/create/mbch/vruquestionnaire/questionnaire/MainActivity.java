package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import dk.aau.create.mbch.vruquestionnaire.NotificationService;
import dk.aau.create.mbch.vruquestionnaire.ParticipantHandler;
import dk.aau.create.mbch.vruquestionnaire.R;
import dk.aau.create.mbch.vruquestionnaire.YesNoDialog;

public class MainActivity extends AppCompatActivity {

    //Private Declarations
    private Handler handler;
    private int taps = 0;
    private long last = 0;
    private static boolean closeOnResume = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        handler = new Handler();

        findViewById(R.id.simulateAccidentButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestion();
            }
        });

        findViewById(R.id.mainTitle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRoutine();
            }
        });

        findViewById(R.id.mainText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                debugRoutine();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Activity activity = this;

        if(closeOnResume) {
            closeOnResume = false;
            finish();
            return;
        }

        assertParticipantData();
        final Context context = this;

        if(NotificationService.isReminderActive()) {
            String dialogText = getString(R.string.main_notificationQuestion);
            int yesTextID =     R.string.notification_answerNow;
            int noTextID  =     R.string.notification_wait;
            DialogInterface.OnClickListener postponeNotification = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NotificationService.postpone(context);
                    activity.finish();
                }
            };
            DialogInterface.OnClickListener continueToQuestion = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    loadQuestion();
                }
            };
            new YesNoDialog(
                    dialogText,
                    yesTextID,
                    noTextID,
                    continueToQuestion,
                    postponeNotification
            ).show(getFragmentManager(), "yesnoReminder");
        }

        System.out.println("Main onResume");
        startService(new Intent(this, NotificationService.class).putExtra(NotificationService.EXTRA_ONLY_ALARM, true));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_newParticipant:
                newUser();
                return true;
            case R.id.action_contact:
                startActivity(new Intent(this, ContactActivity.class));
                return true;
            case R.id.action_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean assertParticipantData() {
        boolean hasParticipantData = hasParticipantData();

        if(!hasParticipantData) {
            newUser();
        }

        return hasParticipantData;
    }

    /**
     * Starts the questionnaire activity
     */
    public void loadQuestion() {
        if(assertParticipantData()) {
            startActivity(new Intent(this, Questionnaire.class).putExtra(Questionnaire.EXTRA_NEW, false));
        }
    }

    /**
     * Starts the questionnaire activity, with the intent of creating a new participant.
     */
    public void newUser() {
        startActivity(new Intent(this, Questionnaire.class).putExtra(Questionnaire.EXTRA_NEW, true));
    }

    /**
     * If called 5 times, with no more than a second to difference between two calls, will delete participant data.
     */
    private void deleteRoutine() {
        //Declarations
        long now = System.currentTimeMillis();

        //Check to see if its time to reset the number of taps
        if(now > last+1000) {
            taps = 0;
        }

        //Set new timestamp
        last = now;

        //Add Tap
        taps++;

        //Check if its time to delete
        if(taps >= 5) {
            //Delete the data!
            if(deleteParticipantData()) {
                CreateToast("Deletion Successful");
            } else {
                CreateToast("Deletion Failed");
            }

            //Reset taps
            taps = 0;
        }
    }

    private void debugRoutine() {
        //Declarations
        long now = System.currentTimeMillis();

        //Check to see if its time to reset the number of taps
        if(now > last+1000) {
            taps = 0;
        }

        //Set new timestamp
        last = now;

        //Add Tap
        taps++;

        //Check if its time to delete
        if(taps >= 5) {
            //Set text field to NotificationArchive
            final TextView tv = (TextView) findViewById(R.id.mainText);
            final String text = NotificationArchive.getText(this);
            tv.post(new Runnable() {
                @Override
                public void run() {
                    tv.setText(text);
                }
            });

            //Reset taps
            taps = 0;
        }
    }

    /**
     * Deletes the data about a participant that is saved locally. Mainly for debugging purposes.
     *
     * @return True if deletion is successful, false otherwise.
     */
    private boolean deleteParticipantData() {
        return ParticipantHandler.deleteParticipantData(this) && NotificationArchive.deleteArchive(this);
    }

    /**
     * Shows a toast message to the screen. Great for debugging purposes.
     * @param toastText The text to be shown in the toast.
     */
    public void CreateToast(final String toastText) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, toastText, duration);
                toast.show();
            }
        });
    }

    /**
     * A check for participant data locally saved on the phone
     * @return True if participant data can be found.
     */
    public boolean hasParticipantData() {
        return ParticipantHandler.participantDataExists(this);
    }

    public static void doCloseOnResume() {
        closeOnResume = true;
    }
}
