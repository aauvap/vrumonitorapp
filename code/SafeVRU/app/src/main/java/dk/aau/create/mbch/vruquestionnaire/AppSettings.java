package dk.aau.create.mbch.vruquestionnaire;

/**
 * Created by mbch on 26/04/16, for the InDev VRU PROJECT
 */

public class AppSettings {
    public int Test_questionnaire_id;
    public int DA_questionnaire_id;
    public int DA_signUp_id;
    public int DA_signUp_emailID;
    public int DA_change_id;
    public int DA_change_emailID;
    public int SE_questionnaire_id;
    public int SE_signUp_id;
    public int SE_signUp_emailID;
    public int SE_change_id;
    public int SE_change_emailID;
    public int ES_questionnaire_id;
    public int ES_signUp_id;
    public int ES_signUp_emailID;
    public int ES_change_id;
    public int ES_change_emailID;
    public int BE_questionnaire_id;
    public int BE_signUp_id;
    public int BE_signUp_emailID;
    public int BE_change_id;
    public int BE_change_emailID;

    public class LocalAppSettings {
        public int questionnaire_id;
        public int signUp_id;
        public int emailID;
        public int test_questionnaire_id;

        public LocalAppSettings(int questionnaire_id, int signUp_id, int emailID, int test_questionnaire_id) {
            this.questionnaire_id = questionnaire_id;
            this.signUp_id = signUp_id;
            this.emailID = emailID;
            this.test_questionnaire_id = test_questionnaire_id;
        }
    }

    public LocalAppSettings getLocalAppSettings(String locale, boolean change) {
        LocalAppSettings res;
        if(locale == null) {
            locale = "";
        }
        int email;
        int signupID;
        switch (locale) {
            default:
                email = change ? DA_change_emailID : DA_signUp_emailID;
                signupID = change ? DA_change_id : DA_signUp_id;
                res = new LocalAppSettings(DA_questionnaire_id, signupID, email, Test_questionnaire_id);
                break;
            case "Sverige":
                email = change ? SE_change_emailID : SE_signUp_emailID;
                signupID = change ? SE_change_id : SE_signUp_id;
                res = new LocalAppSettings(SE_questionnaire_id, signupID, email, Test_questionnaire_id);
                break;
            case "Catalunya":
                email = change ? ES_change_emailID : ES_signUp_emailID;
                signupID = change ? ES_change_id : ES_signUp_id;
                res = new LocalAppSettings(ES_questionnaire_id, signupID, email, Test_questionnaire_id);
                break;
            case "België":
                email = change ? BE_change_emailID : BE_signUp_emailID;
                signupID = change ? BE_change_id : BE_signUp_id;
                res = new LocalAppSettings(BE_questionnaire_id, signupID, email, Test_questionnaire_id);
                break;
        }

        return res;
    }
}