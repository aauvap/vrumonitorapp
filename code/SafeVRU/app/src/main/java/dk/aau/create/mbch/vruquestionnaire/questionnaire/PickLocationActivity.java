package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import dk.aau.create.mbch.vruquestionnaire.R;

public class PickLocationActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener{

    private GoogleMap map;
    private ArrayList<Marker> markers = new ArrayList<>();
    private Handler handler;
    private int maxPoints;
    private double[] lats;
    private double[] longs;
    private float zoom = 16;
    private LocationManager locationManager;

    private final int requestCode = 234;

    private static LatLng curLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        setContentView(R.layout.activity_pick_location);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Intent intent = getIntent();

        if(curLatLng == null) {
            if(isLocationEnabled(this)) {
                requestPosition();
            } else {
                zoom = 6.8f;
                loadMap(56.0166288, 10.3224842);
            }
        } else {
            loadMap(curLatLng.latitude, curLatLng.longitude);
        }

        maxPoints = intent.getIntExtra("maxPoints", 1);
        lats  = intent.getDoubleArrayExtra("Lat");
        longs = intent.getDoubleArrayExtra("Lng");

        if (lats == null) {
            lats = new double[0];
        }
        if (longs == null) {
            longs = new double[0];
        }
    }

    private void requestPosition() {
        String[] permission = new String[] {Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions(this, permission, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        System.out.println("onRequestPermissionsResult");
        if(requestCode == this.requestCode) {
            if(grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                try {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                    return;
                } catch (SecurityException e) {
                    CreateToast(e.getMessage());
                }
            }
        }

        zoom = 6.8f;
        loadMap(56.0166288, 10.3224842);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        final int normalMap = GoogleMap.MAP_TYPE_NORMAL;
        final int satMap = GoogleMap.MAP_TYPE_HYBRID;

        for(int i = 0; i < lats.length; i++) {
            LatLng newPos = new LatLng(lats[i], longs[i]);
            if(i == 0) {
                curLatLng = newPos;
            }

            Marker marker = map.addMarker(new MarkerOptions()
                    .position(newPos)
                    .draggable(true));
            markers.add(marker);
        }

        map.moveCamera(CameraUpdateFactory.zoomTo(zoom));
        map.moveCamera(CameraUpdateFactory.newLatLng(curLatLng));
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (markers.size() < maxPoints) {
                    Marker marker = map.addMarker(new MarkerOptions()
                            .position(latLng)
                            .draggable(true));
                    markers.add(marker);
                } else {
                    CreateToast(getString(R.string.pickLocation_tooManyMarkers));
                }
            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                markers.remove(marker);
                marker.remove();
                return false;
            }
        });



        map.setMapType(normalMap);
        findViewById(R.id.switchModeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int newType = map.getMapType() == normalMap
                        ? satMap
                        : normalMap;

                map.setMapType(newType);
            }
        });

        map.getUiSettings().setMapToolbarEnabled(false);
    }

    public void onOKButton(View view) {
        double[] latitudes = new double[markers.size()];
        double[] longitudes = new double[markers.size()];

        for(int i = 0; i < markers.size(); i++) {
            Marker marker = markers.get(i);
            latitudes[i] = marker.getPosition().latitude;
            longitudes[i] = marker.getPosition().longitude;
        }

        Intent resultIntent = new Intent()
                .putExtra("latitudes", latitudes)
                .putExtra("longitudes", longitudes);

        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void loadMap(double lat, double lng) {
        System.out.println("Location Changed!");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        curLatLng = new LatLng(lat, lng);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        findViewById(R.id.pickLocationFrame).setVisibility(View.GONE);
    }

    public void CreateToast(final String toastText) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, toastText, duration);
                toast.show();
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        loadMap(location.getLatitude(), location.getLongitude());
        zoom = 16;

        try {
            locationManager.removeUpdates(this);
        } catch (SecurityException ignored) {}
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }


}
