package dk.aau.create.mbch.vruquestionnaire.answers;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by mbch on 23/10/15.
 */
@SuppressWarnings("FieldCanBeLocal")
public class MapAnswer extends Answer {
    private int map_field_id;
    private double[] latitudes;
    private double[] longitudes;
    //private double orientation;

    public MapAnswer(int map_field_id, double[] latitudes, double[] longitudes) {
        super(FieldType.Map);
        this.map_field_id = map_field_id;
        this.latitudes = latitudes;
        this.longitudes = longitudes;
    }

    public double[] getLatitudes() {
        return latitudes;
    }

    public double[] getLongitudes() {
        return longitudes;
    }

}
