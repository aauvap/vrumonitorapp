package dk.aau.create.mbch.vruquestionnaire;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.LocaleHandler;

public class LocalizationDialog extends DialogFragment {
    private Context context;

    public LocalizationDialog(Context context) {
        this.context = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context context = this.context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_localechoice, null);

        final Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.country_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        builder.setView(view);
        builder.setPositiveButton(R.string.pickLocation_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String[] countries = getResources().getStringArray(R.array.country_array);
                String result = countries[((int) spinner.getSelectedItemId())];
                LocaleHandler.setCurrentLocale(result);
                synchronized (context) {
                    context.notify();
                }
            }
        });

        this.setCancelable(false);
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}
