package dk.aau.create.mbch.vruquestionnaire.questionnaire;

/**
 * Created by Mads on 15-09-2015.
 */
public enum FieldType {
    MultipleChoice,
    Number,
    FreeText,
    Date,
    Map,
}
