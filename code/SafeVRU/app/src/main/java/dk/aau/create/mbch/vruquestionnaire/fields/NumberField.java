package dk.aau.create.mbch.vruquestionnaire.fields;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by Mads on 15-10-2015.
 */
@SuppressWarnings("unused")
public class NumberField extends QuestionField {
    private int min_number;
    private int max_number;
    private boolean allow_floats;

    public int getMin_number() {
        return min_number;
    }

    public int getMax_number() {
        return max_number;
    }

    public boolean isAllow_floats() {
        return allow_floats;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.Number;
    }
}
