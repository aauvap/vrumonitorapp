package dk.aau.create.mbch.vruquestionnaire.fields;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by Mads on 15-10-2015.
 */
@SuppressWarnings("unused")
public class MapField extends QuestionField {
    private int min_num_marks;
    private int max_num_marks;

    public int getMin_num_marks() {
        return min_num_marks;
    }

    public int getMax_num_marks() {
        return max_num_marks;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.Map;
    }
}
