package dk.aau.create.mbch.vruquestionnaire;

import android.content.Context;

import com.squareup.okhttp.CacheControl;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * Created by mbch on 28/10/15.
 */
public class WebInterfaceConnection {
    private static boolean isSetup = false;
    private Context context;
    private OkHttpClient client = new OkHttpClient();
    private CacheControl cacheControl;

    private void setup() {
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(context.getString(R.string.username), context.getString(R.string.password).toCharArray());
            }
        });
        isSetup = true;
    }

    public WebInterfaceConnection(Context context) {
        if(!isSetup) {
            setup();
        }
        this.context = context;
        cacheControl = new CacheControl.Builder()
                .noCache()
                .build();
    }

    public String getTextFromURL(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .cacheControl(cacheControl)
                .build();

        Response response = client.newCall(request).execute();
        System.out.println("get url: " + url + "; response.message: " + response.message());
        return response.body().string();
    }

    public String postDataToURL(String url, String jsonBody) throws IOException {
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, jsonBody);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println("post url: " + url + "; response.message: " + response.message());
        return response.body().string();
    }
}
