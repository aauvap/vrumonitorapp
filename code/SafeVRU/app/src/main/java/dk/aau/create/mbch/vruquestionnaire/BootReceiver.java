package dk.aau.create.mbch.vruquestionnaire;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // an Intent broadcast.
        System.out.println("Boot Received, SAFEVRU");
        context.startService(new Intent(context, NotificationService.class));
    }
}
