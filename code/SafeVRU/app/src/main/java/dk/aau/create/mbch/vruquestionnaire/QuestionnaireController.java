package dk.aau.create.mbch.vruquestionnaire;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import dk.aau.create.mbch.vruquestionnaire.answers.*;
import dk.aau.create.mbch.vruquestionnaire.fields.FreeTextField;
import dk.aau.create.mbch.vruquestionnaire.questionnaire.LocaleHandler;
import dk.aau.create.mbch.vruquestionnaire.questionnaire.MainActivity;
import dk.aau.create.mbch.vruquestionnaire.questionnaire.Question;
import dk.aau.create.mbch.vruquestionnaire.questionnaire.Questionnaire;

public class QuestionnaireController implements Runnable{
    private Questionnaire questionnaire;
    private boolean newParticipant;
    private boolean testRun;
    private String questionURL = "https://vru.create.aau.dk/questions/json/";
    private String endScreenURL = "https://vru.create.aau.dk/questionnaires/end/";
    private WebInterfaceConnection webInterfaceConnection;
    private int currentQuestionID;
    private int questionnaireID;
    private int incidentID;
    private int previousQuestionID;
    private String introText;
    private String incidentKey;
    private Gson gson;
    private final Object lock = new Object();
    private AppSettings.LocalAppSettings localAppSettings;
    private boolean createUserAtEnd = false;


    public QuestionnaireController(final Questionnaire questionnaire, boolean newParticipant, boolean testRun) {
        this.questionnaire = questionnaire;
        this.newParticipant = newParticipant;
        this.testRun = testRun;
        webInterfaceConnection = new WebInterfaceConnection(questionnaire);
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {
        try {
            //Declarations
            String usedURL;
            ParticipantData participantData = ParticipantHandler.loadParticipantData(questionnaire);

            if(newParticipant && participantData == null) {
                launchLocalizationDialog();
            } else {
                LocaleHandler.setCurrentLocale(participantData.locale);
            }

            String locale = LocaleHandler.getCurrentLocale();
            AppSettings appSettings = AppSettingsHandler.getAppSettings(questionnaire);
            localAppSettings = appSettings.getLocalAppSettings(locale, participantData != null);

            //Set the questionnaireID
            questionnaireID = newParticipant
                    ? localAppSettings.signUp_id
                    : participantData.questionnaireID;

            //Detemine the url for creating the incident
            if(newParticipant) {
                String newParticipantURL = "https://vru.create.aau.dk/questionnaires/new_participant/";
                usedURL = newParticipantURL +questionnaireID + ".json";
                if(participantData != null) {
                    usedURL += "?id=" + participantData.id;
                }
            } else {
                String newIncidentURL = "https://vru.create.aau.dk/questionnaires/new_incident/";
                usedURL = newIncidentURL + participantData.id + ".json?key=" + participantData.key;
            }

            //Get response from usedURL
            String response = webInterfaceConnection.getTextFromURL(usedURL);
            System.out.println("titleInfo response: " + response);
            TitleInfo titleInfo = gson.fromJson(response, TitleInfo.class);

            if(titleInfo.error != null) {
                questionnaire.CreateToast("Error in connecting to server. Try again later! Error Code: " + 32);
                questionnaire.finish();
                return;
            }

            introText = titleInfo.intro_text;

            response = webInterfaceConnection.postDataToURL(usedURL, gson.toJson(new SensorDump()));
            IncidentInfo incidentInfo = gson.fromJson(response, IncidentInfo.class);

            currentQuestionID = incidentInfo.firstQuestion;
            incidentID = incidentInfo.incidentId;
            incidentKey = incidentInfo.incidentKey;

            if(introText.equals("")){
                loadCurrentQuestion(questionnaire);
            } else {
                questionnaire.loadStartScreen(introText);
            }

        } catch (IOException e) {
            e.printStackTrace();
            questionnaire.CreateToast(questionnaire.getString(R.string.system_noConnection));
            questionnaire.finish();
        }
    }

    public void loadCurrentQuestion(final Questionnaire questionnaire) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String usedURL = questionURL+ currentQuestionID + "/" + incidentID +".json?key=" + incidentKey;
                    String response = webInterfaceConnection.getTextFromURL(usedURL);
                    System.out.println("Received Question: " + response);
                    ReceivedQuestion receivedQuestion= gson.fromJson(response, ReceivedQuestion.class);
                    Question question = receivedQuestion.question;
                    for(FreeTextField ftf : question.getFree_text_fields()) {
                        if(ftf.getId() == localAppSettings.emailID) {
                            createUserAtEnd = true;
                        }
                    }

                    previousQuestionID = receivedQuestion.getPreviousQuestion();
                    questionnaire.loadQuestion(question);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void loadPreviousQuestion(Questionnaire questionnaire) {
        if(previousQuestionID > 0) {
            currentQuestionID = previousQuestionID;
            loadCurrentQuestion(questionnaire);
        } else {
            questionnaire.loadStartScreen(introText);
        }
    }

    public void receiveAnswer(final Answer[] answers, final Questionnaire questionnaire) {
        final String text2write = gson.toJson(new OutputClass(answers));
        String postTarget = "https://vru.create.aau.dk/questions/save_answers/";
        final String url = postTarget + currentQuestionID + "/" + incidentID + ".json?key=" + incidentKey;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response = webInterfaceConnection.postDataToURL(url, text2write);
                    int nextQuestion = gson.fromJson(response, PostResponse.class).nextQuestion;
                    if(nextQuestion >= 0) {
                        currentQuestionID = nextQuestion;
                        loadCurrentQuestion(questionnaire);
                    } else {
                        //String usedURL = endScreenURL+questionnaireID+"/"+incidentID+".json?key="+incidentKey;
                        int targetQuestionnaireID = testRun
                                ? localAppSettings.test_questionnaire_id
                                : localAppSettings.questionnaire_id;


                        int fieldIDForEmail = localAppSettings.emailID;
                        String usedURL = newParticipant
                                ? endScreenURL + questionnaireID + "/" + incidentID + ".json?key=" + incidentKey + "&target_questionnaire="+targetQuestionnaireID+"&email_field=" + fieldIDForEmail
                                : endScreenURL+questionnaireID+"/"+incidentID+".json?key="+incidentKey;

                        previousQuestionID = currentQuestionID;
                        response = webInterfaceConnection.getTextFromURL(usedURL);



                        EndInfo endInfo = gson.fromJson(response, EndInfo.class);
                        if(newParticipant) {
                            if(createUserAtEnd) {
                                ParticipantHandler.saveParticipantData(questionnaire, endInfo.participantId, endInfo.participantKey, targetQuestionnaireID, LocaleHandler.getCurrentLocale());
                            } else {
                                MainActivity.doCloseOnResume();
                            }
                        } else {
                            NotificationService.reportQuestionnaireFinished(questionnaire);
                        }

                        questionnaire.loadEndScreen(endInfo.end_text);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private class SensorDump {
        public String timeFromApp;

        public SensorDump() {
            timeFromApp = new Date(System.currentTimeMillis()).toString();
        }
    }

    private class ReceivedQuestion {
        public Question question;
        public String previousQuestion;


        public int getPreviousQuestion() {
            int res = -1;
            if(!previousQuestion.isEmpty()) {
                try {
                    res = Integer.parseInt(previousQuestion);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            return res;
        }
    }

    private class PostResponse {
        public int nextQuestion;
    }

    private class TitleInfo {
        public String title;
        public String intro_text;
        public String error;
    }

    private class EndInfo {
        public String title;
        public String end_text;
        public int participantId;
        public String participantKey;
    }

    private class IncidentInfo {
        public int incidentId;
        public String incidentKey;
        public int firstQuestion;
    }



    public class OutputClass {
        public DateAnswer[] DateFields;
        public FreeTextAnswer[] FreeTextFields;
        public MapAnswer[] MapFields;
        public MultipleChoiceAnswer[] MultipleChoiceFields;
        public NumberAnswer[] NumberAnswers;

        public OutputClass(Answer[] answers) {
            ArrayList<DateAnswer> dateList = new ArrayList<>();
            ArrayList<FreeTextAnswer> textList = new ArrayList<>();
            ArrayList<MapAnswer> mapList = new ArrayList<>();
            ArrayList<MultipleChoiceAnswer> multiList = new ArrayList<>();
            ArrayList<NumberAnswer> numberList = new ArrayList<>();

            for(Answer answer : answers) {
                switch (answer.getFieldType()) {
                    case Date:
                        dateList.add((DateAnswer) answer);
                        break;
                    case FreeText:
                        textList.add((FreeTextAnswer) answer);
                        break;
                    case Map:
                        mapList.add((MapAnswer) answer);
                        break;
                    case MultipleChoice:
                        multiList.add((MultipleChoiceAnswer) answer);
                        break;
                    case Number:
                        numberList.add((NumberAnswer) answer);
                        break;
                }
            }

            DateFields = new DateAnswer[dateList.size()];
            FreeTextFields = new FreeTextAnswer[textList.size()];
            MapFields = new MapAnswer[mapList.size()];
            MultipleChoiceFields = new MultipleChoiceAnswer[multiList.size()];
            NumberAnswers = new NumberAnswer[numberList.size()];

            DateFields = dateList.toArray(DateFields);
            FreeTextFields = textList.toArray(FreeTextFields);
            MapFields = mapList.toArray(MapFields);
            MultipleChoiceFields = multiList.toArray(MultipleChoiceFields);
            NumberAnswers = numberList.toArray(NumberAnswers);
        }
    }

    private void launchLocalizationDialog() {
        LocaleHandler.determineLocale(questionnaire);
        synchronized (questionnaire){
            try {
                questionnaire.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
