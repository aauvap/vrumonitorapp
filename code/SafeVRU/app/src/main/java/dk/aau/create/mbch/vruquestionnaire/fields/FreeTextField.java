package dk.aau.create.mbch.vruquestionnaire.fields;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by Mads on 15-10-2015.
 */
@SuppressWarnings("unused")
public class FreeTextField extends QuestionField {
    private String sample_text;
    private int min_length;
    private int max_length;
    private boolean validate_as_email;

    public String getSample_text() {
        return sample_text;
    }

    public int getMin_length() {
        return min_length;
    }

    public int getMax_length() {
        return max_length;
    }

    public boolean isValidate_as_email() {
        return validate_as_email;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.FreeText;
    }
}
