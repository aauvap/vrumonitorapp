package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import dk.aau.create.mbch.vruquestionnaire.ParticipantData;
import dk.aau.create.mbch.vruquestionnaire.ParticipantHandler;
import dk.aau.create.mbch.vruquestionnaire.R;

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        ParticipantData participantData = ParticipantHandler.loadParticipantData(this);
        TextView tv = (TextView) findViewById(R.id.userIDText);
        String text2set;

        if(participantData == null) {
            text2set = "";
        } else {
            text2set = getString(R.string.contact_userIDText, participantData.id);
        }

        tv.setText(text2set);
    }
}
