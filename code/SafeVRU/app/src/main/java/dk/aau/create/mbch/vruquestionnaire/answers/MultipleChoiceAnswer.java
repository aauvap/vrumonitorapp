package dk.aau.create.mbch.vruquestionnaire.answers;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by mbch on 23/10/15.
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class MultipleChoiceAnswer extends Answer {
    private int[] multiple_choice_option_id;

    public MultipleChoiceAnswer(int[] multiple_choice_option_id) {
        super(FieldType.MultipleChoice);
        this.multiple_choice_option_id = multiple_choice_option_id;
    }

    public int[] getMultiple_choice_option_id() {
        return multiple_choice_option_id;
    }

}
