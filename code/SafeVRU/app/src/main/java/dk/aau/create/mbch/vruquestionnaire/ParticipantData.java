package dk.aau.create.mbch.vruquestionnaire;

/**
 * Created by mbch on 26/04/16, for the InDev VRU PROJECT
 */
public class ParticipantData {
    public int id;
    public String key;
    public int questionnaireID;
    public String locale;

    public ParticipantData(int id, String key, int questionnaireID, String locale) {
        this.id = id;
        this.key = key;
        this.questionnaireID = questionnaireID;
        this.locale = locale;
    }
}