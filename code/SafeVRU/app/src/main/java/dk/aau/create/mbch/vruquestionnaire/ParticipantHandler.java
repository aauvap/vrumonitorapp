package dk.aau.create.mbch.vruquestionnaire;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mbch on 26/04/16, for the InDev VRU PROJECT
 */
public class ParticipantHandler {
    private static final String participantDataFileName = "pd.txt";

    public static boolean saveParticipantData(Context context, int id, String key, int questionID, String locale) {
        Gson gson = new Gson();
        try {
            FileOutputStream stream = context.openFileOutput(participantDataFileName, Context.MODE_PRIVATE);
            ParticipantData participantData = new ParticipantData(id, key, questionID, locale);
            stream.write(gson.toJson(participantData).getBytes());
            stream.close();
            context.startService(
                    new Intent(context, NotificationService.class)
                        .putExtra(NotificationService.EXTRA_ONLY_ALARM, true)
            );
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Nullable
    public static ParticipantData loadParticipantData(Context context) {
        Gson gson = new Gson();
        File file = new File (context.getFilesDir(), participantDataFileName);
        if(file.exists()) {
            try {
                FileInputStream stream = context.openFileInput(participantDataFileName);
                byte[] buffer = new byte[stream.available()];
                stream.read(buffer);
                String content = new String(buffer);
                return gson.fromJson(content, ParticipantData.class);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public static boolean deleteParticipantData(Context context) {
        File file = new File(context.getFilesDir(), participantDataFileName);
        return file.delete();
    }

    public static boolean participantDataExists(Context context) {
        File file = new File(context.getFilesDir(), participantDataFileName);
        return file.exists();
    }
}
