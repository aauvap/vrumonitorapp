package dk.aau.create.mbch.vruquestionnaire.fields;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by Mads on 15-10-2015.
 */
public abstract class QuestionField {
    protected int id;
    protected int questionnaire_id;
    protected String caption;
    protected int priority;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionnaire_id() {
        return questionnaire_id;
    }

    public String getCaption() {
        return caption;
    }

    public int getPriority() {
        return priority;
    }
    public abstract FieldType getFieldType();
}
