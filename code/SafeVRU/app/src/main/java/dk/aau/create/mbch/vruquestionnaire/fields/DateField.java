package dk.aau.create.mbch.vruquestionnaire.fields;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by Mads on 15-10-2015.
 */
@SuppressWarnings("unused")
public class DateField extends QuestionField {
    private String min_date;
    private String max_date;
    private boolean allow_time;
    private transient DateFormat formatter;
    private final String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";

    public Date getMax_date() throws ParseException {
        //2015-12-01T00:00:00+0000
        if(formatter == null) {
            formatter = new SimpleDateFormat(dateFormat);
        }
        if(max_date != null) {
            return formatter.parse(max_date);
        }

        return null;
    }

    public boolean isAllow_time() {
        return allow_time;
    }

    public Date getMin_date() throws ParseException {
        if(formatter == null) {
            formatter = new SimpleDateFormat(dateFormat);
        }
        if(min_date != null) {
            return formatter.parse(min_date);
        }

        return null;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.Date;
    }
}
