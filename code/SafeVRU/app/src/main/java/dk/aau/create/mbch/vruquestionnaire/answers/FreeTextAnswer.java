package dk.aau.create.mbch.vruquestionnaire.answers;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by mbch on 23/10/15.
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class FreeTextAnswer extends Answer{
    private int free_text_field_id;
    private String text;

    public FreeTextAnswer(int free_text_field_id, String text) {
        super(FieldType.FreeText);
        this.free_text_field_id = free_text_field_id;
        this.text = text;
    }

    public int getFree_text_field_id() {
        return free_text_field_id;
    }

    public String getText() {
        return text;
    }

}
