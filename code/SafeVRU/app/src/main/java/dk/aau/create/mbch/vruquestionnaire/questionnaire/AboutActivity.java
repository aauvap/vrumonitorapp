package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import dk.aau.create.mbch.vruquestionnaire.AppSettingsHandler;
import dk.aau.create.mbch.vruquestionnaire.ParticipantData;
import dk.aau.create.mbch.vruquestionnaire.ParticipantHandler;
import dk.aau.create.mbch.vruquestionnaire.R;

public class AboutActivity extends AppCompatActivity {
    private int taps = 0;
    private long last = 0;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        handler = new Handler();

        findViewById(R.id.euflag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testRoutine();
            }
        });
    }

    private void testRoutine() {
        //Declarations
        long now = System.currentTimeMillis();

        //Check to see if its time to reset the number of taps
        if(now > last+1000) {
            taps = 0;
        }

        //Set new timestamp
        last = now;

        //Add Tap
        taps++;

        //Check if its time to test_questionnaire_id
        if(taps >= 10) {
            final Context context = this;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(context, Questionnaire.class);
                    intent.putExtra(Questionnaire.EXTRA_NEW, true);
                    intent.putExtra(Questionnaire.EXTRA_TEST, true);
                    startActivity(intent);
                }
            }).start();
            //Reset taps
            taps = 0;
        }
    }

    public void CreateToast(final String toastText) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, toastText, duration);
                toast.show();
            }
        });
    }
}
