package dk.aau.create.mbch.vruquestionnaire.questionnaire;

import android.app.Activity;

import dk.aau.create.mbch.vruquestionnaire.LocalizationDialog;

/**
 * Created by mbch on 27/07/16, for the InDev VRU PROJECT
 */
public class LocaleHandler {
    private static String currentLocale = null;

    public static void determineLocale(Activity activity) {
        new LocalizationDialog(activity).show(activity.getFragmentManager(), "localeDialog");
    }

    public static String getCurrentLocale() {
        return currentLocale;
    }

    public static void setCurrentLocale(String currentLocale) {
        LocaleHandler.currentLocale = currentLocale;
    }
}
