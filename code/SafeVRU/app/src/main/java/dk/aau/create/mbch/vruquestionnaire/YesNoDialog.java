package dk.aau.create.mbch.vruquestionnaire;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by mbch on 26/04/16, for the InDev VRU PROJECT
 */
public class YesNoDialog extends DialogFragment {
    private String message;
    private int textIDYes;
    private int textIDNo;
    private DialogInterface.OnClickListener onYes;
    private DialogInterface.OnClickListener onNo;

    public YesNoDialog(String message, DialogInterface.OnClickListener onYes, DialogInterface.OnClickListener onNo) {
        this.message = message;
        this.onYes = onYes;
        this.onNo = onNo;
        textIDYes = R.string.general_yes;
        textIDNo = R.string.general_no;
    }

    public YesNoDialog(String message, int textIDYes, int textIDNo, DialogInterface.OnClickListener onYes, DialogInterface.OnClickListener onNo) {
        this.message = message;
        this.textIDYes = textIDYes;
        this.textIDNo = textIDNo;
        this.onYes = onYes;
        this.onNo = onNo;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setPositiveButton(textIDYes, onYes)
                .setNegativeButton(textIDNo, onNo);
        // Create the AlertDialog object and return it
        this.setCancelable(false);
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}