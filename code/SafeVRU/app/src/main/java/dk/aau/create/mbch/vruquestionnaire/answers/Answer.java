package dk.aau.create.mbch.vruquestionnaire.answers;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

/**
 * Created by mbch on 23/10/15.
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public abstract class Answer {
    private transient FieldType fieldType;

    public Answer(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

}
