package dk.aau.create.mbch.vruquestionnaire.answers;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

import dk.aau.create.mbch.vruquestionnaire.R;
import dk.aau.create.mbch.vruquestionnaire.questionnaire.FieldType;

@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class DateAnswer extends Answer {
    private int date_field_id;
    private String datetime;
    private boolean know_time;
    private boolean know_date;
    private transient Date date;


    public DateAnswer(Context context, int date_field_id, Date date, boolean knowTime, boolean knowDate) {
        super(FieldType.Date);
        this.date_field_id = date_field_id;
        this.date = date;
        this.know_time = knowTime;
        this.know_date = knowDate;

        java.text.DateFormat formatter = new SimpleDateFormat(context.getString(R.string.date_format));
        datetime = formatter.format(date);
        //this.datetime = datetime;
    }

    public Date getDate() {
        return date;
    }

}
