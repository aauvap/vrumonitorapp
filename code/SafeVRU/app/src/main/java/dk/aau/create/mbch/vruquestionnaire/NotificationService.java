package dk.aau.create.mbch.vruquestionnaire;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;
import java.util.Calendar;

import dk.aau.create.mbch.vruquestionnaire.questionnaire.MainActivity;
import dk.aau.create.mbch.vruquestionnaire.questionnaire.NotificationArchive;

public class NotificationService extends Service {
    private static int notificationID = 5849;
    public static final String POSTPONE_EXTRA = "postpone";

    private static boolean reminderActive = false;

    public final static String EXTRA_ONLY_ALARM = "onlyalarmplease";

    public NotificationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Notification Service Started");
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar nowCalendar = Calendar.getInstance();
        Calendar otherCalendar = Calendar.getInstance();
        final Context context = this;

        otherCalendar.set(Calendar.HOUR_OF_DAY, 12);
        otherCalendar.set(Calendar.MINUTE, 0);
        if(otherCalendar.getTimeInMillis() < nowCalendar.getTimeInMillis()) {
            otherCalendar.add(Calendar.DATE, 1);
        }

        long alarmMillis = otherCalendar.getTimeInMillis();
        PendingIntent pendingIntent = PendingIntent.getService(this, 38394, new Intent(this, NotificationService.class), 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmMillis, 24 * 60 * 60 * 1000, pendingIntent);
        System.out.println("Normal alarm set for: " + otherCalendar.getTime());

        if(!intent.getBooleanExtra(EXTRA_ONLY_ALARM, false) && !timeOutOfBounds(nowCalendar)) {
            int hour = nowCalendar.get(Calendar.HOUR_OF_DAY);
            if(hour > 8 && hour <= 17) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Notification Thread Started");
                        for (int k = 0; k < 10; k++) {
                            try {
                                System.out.println("participantDataExists: " + ParticipantHandler.participantDataExists(context));
                                System.out.println("monthActive: " + NotificationArchive.monthActive(context));
                                if(ParticipantHandler.participantDataExists(context) && NotificationArchive.monthActive(context)) {
                                    PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationID, new Intent(context, MainActivity.class), 0);

                                    Notification notification = new NotificationCompat.Builder(context)
                                            .setContentText(getString(R.string.notification_text))
                                            .setContentTitle(getString(R.string.notification_title))
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setAutoCancel(true)
                                            .setContentIntent(pendingIntent)
                                            .setVibrate(new long[]{0L, 1000L})
                                            .build();

                                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                    reminderActive = true;
                                    notificationManager.notify(notificationID, notification);
                                    NotificationArchive.modifyMonth(context, false);
                                }
                                k = 10;
                                System.out.println("Finished the try block");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        stopSelf();
                    }
                });
                thread.start();
            }
        }

        return START_NOT_STICKY;
    }

    public static boolean isReminderActive() {
        return reminderActive;
    }

    public static void reportQuestionnaireFinished(Context context) {
        if(reminderActive) {
            NotificationArchive.modifyMonth(context, true);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getService(context, 38395, new Intent(context, NotificationService.class), 0);
            alarmManager.cancel(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationID);
        }
        reminderActive = false;
    }

    public static void postpone(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        Calendar newTime = Calendar.getInstance();
        newTime.add(Calendar.HOUR_OF_DAY, 3);
        if(timeOutOfBounds(newTime)) {
            if(newTime.get(Calendar.HOUR_OF_DAY) >= 17) {
                newTime.add(Calendar.DATE, 1);
            }
            newTime.set(Calendar.HOUR_OF_DAY, 9);
        }

        long millis = newTime.getTimeInMillis();

        PendingIntent pendingIntent = PendingIntent.getService(context, 38395, new Intent(context, NotificationService.class), 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, millis, pendingIntent);
        System.out.println("Postpone alarm set for: " + newTime.getTime());
    }

    private static boolean timeOutOfBounds(Calendar calendar) {
        return calendar.get(Calendar.HOUR_OF_DAY) >= 17 || calendar.get(Calendar.HOUR_OF_DAY) < 8;
    }
}