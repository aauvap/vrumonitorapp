package dk.mbock.detectionsystem;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private int requestCode = 1337;
    private long lastTap = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Main");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());

        findViewById(R.id.contactUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startContactScreen();
            }
        });

        findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.statusText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long current = System.currentTimeMillis();
                if ((current - lastTap) < 1000) {
                    startService(new Intent(CustomApplication.getAppContext(), UploadService.class));
                } else {
                    lastTap = current;
                }
            }
        });

        AlarmManager systemService = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent = BootReceiver.getPendingIntent(this);
        systemService.cancel(pendingIntent);
        systemService.setInexactRepeating(AlarmManager.RTC, 0, 1000 * 60 * 60, pendingIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        boolean hasAcc = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null;
        boolean hasGyro= sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null;
        if(!(hasAcc && hasGyro)) {
            InstallLogHandler.handleInstallLog(false);
            startActivity(new Intent(this, UnusableActivity.class));
            finish();
            return;
        }
        if(SettingsActivity.getUserData() == null) {
            startInfoScreen();
        } else if(!DetectionService.running) {
            startService(new Intent(this, DetectionService.class));
        }
    }

    private void startInfoScreen() {
        Intent newIntent = new Intent(this, SettingsActivity.class);

        TaskStackBuilder.create(this)
                .addNextIntentWithParentStack(newIntent)
                .startActivities();
    }

    private void startContactScreen() {
        startActivity(new Intent(CustomApplication.getAppContext(), ContactActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("RequestCode: " + requestCode + "; resultCode: " + resultCode);
        if(this.requestCode == requestCode) {
            startService(new Intent(this, DetectionService.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case R.id.button_menu_settings:
                startActivityForResult(new Intent(this, SettingsActivity.class), requestCode);
                break;
            case R.id.button_menu_info:
                startActivity(new Intent(this, InfoActivity.class));
                break;
            case R.id.button_menu_contact:
                startContactScreen();
                break;
        }
        return false;
    }


}
