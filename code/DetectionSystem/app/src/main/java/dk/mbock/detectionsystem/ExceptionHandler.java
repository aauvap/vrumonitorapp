package dk.mbock.detectionsystem;

import android.content.Context;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mbch on 05/04/16, for the InDev VRU PROJECT
 */
public final class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override  public void uncaughtException(Thread aThread, Throwable aThrowable) {
        Context context = CustomApplication.getAppContext();
        File file = new File(context.getFilesDir(), context.getString(R.string.system_errorListFile));

        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
            outputStream.write(aThrowable.getMessage().getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
