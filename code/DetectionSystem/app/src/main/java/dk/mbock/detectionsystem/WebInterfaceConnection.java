package dk.mbock.detectionsystem;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import okhttp3.CacheControl;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by mbch on 28/10/15, for the InDev VRU PROJECT
 */
@SuppressWarnings("FieldCanBeLocal")
class WebInterfaceConnection {
    private static boolean isSetup = false;
    private OkHttpClient client = new OkHttpClient();
    private CacheControl cacheControl;
    private final String appSettingsDir = "appSettings";

    private final String appSettingsURL = "https://vru.create.aau.dk/app_settings.json";

    private void setup() {
        isSetup = true;
    }

    public WebInterfaceConnection() {
        if(!isSetup) {
            setup();
        }

        cacheControl = new CacheControl.Builder()
                .noCache()
                .build();
    }

    private String getTextFromURL(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .cacheControl(cacheControl)
                .build();

        Response response = client.newCall(request).execute();
        System.out.println("get response.message: " + response.message());
        return response.body().string();
    }

    public AppSettings getAppSettings(Context context) {
        Gson gson = new Gson();
        try {
            String response = getTextFromURL(appSettingsURL);
            String output = gson.fromJson(response, SettingsOutput.class).appSettings;
            saveAppSettings(context, output);
            return gson.fromJson(output, AppSettings.class);
        }
        catch (IOException | JsonSyntaxException e) {
            e.printStackTrace();
        }

        return gson.fromJson(loadAppSettings(context), AppSettings.class);
    }

    public String postTextFromURL(String url, String[] keys, String[] values) throws IOException {
        FormBody.Builder builder = new FormBody.Builder();

        for(int i = 0; i < keys.length; i++) {
            builder.add(keys[i], values[i]);
        }

        RequestBody formBody = builder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        return response.body().string();
    }

    @SuppressWarnings("UnusedReturnValue")
    private boolean saveAppSettings(Context context, String appSettings) {
        File file = new File(context.getFilesDir(), appSettingsDir+".txt");

        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file, false));
            stream.write(appSettings.getBytes());
            stream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private String loadAppSettings(Context context) {
        File file = new File(context.getFilesDir(), appSettingsDir+".txt");

        if(file.exists()) {
            try {
                FileInputStream stream = context.openFileInput(appSettingsDir+".txt");
                byte[] buffer = new byte[stream.available()];
                stream.read(buffer);
                return new String (buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public class SettingsOutput {
        public String appSettings;
    }
}
