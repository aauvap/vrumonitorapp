package dk.mbock.detectionsystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class SettingsActivity extends AppCompatActivity implements TextWatcher, Runnable{

    private static String userdataFilename = "UserData.txt";

    private EditText emailField;
    private EditText phoneField;
    private EditText confirmField;
    private Button registerButton;

    private static Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        emailField = (EditText) findViewById(R.id.emailField);
        confirmField = (EditText) findViewById(R.id.emailFieldConfirm);
        phoneField = (EditText) findViewById(R.id.phoneNumberField);
        registerButton = (Button) findViewById(R.id.registerButton);

        emailField.addTextChangedListener(this);
        confirmField.addTextChangedListener(this);
        phoneField.addTextChangedListener(this);

        UserData userData = getUserData();
        if(userData != null) {
            emailField.setText(userData.email);
            phoneField.setText(userData.phoneNumber);
        }

        final SettingsActivity settingsActivity = this;

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields(emailField.getText().toString(),emailField.getText().toString(), phoneField.getText().toString())) {
                    new Thread(settingsActivity).start();
                }
            }
        });

        if(getUserData() == null) {
            startActivity(new Intent(this, InfoActivity.class));
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPhoneNumber(String number) {
        return !Objects.equals(number, "") && Patterns.PHONE.matcher(number).matches();
    }

    private static boolean validateFields(String email, String confirm, String phone) {
        //Confirm Email
        boolean confirmed = email.equals(confirm);

        //Validate Email
        boolean emailValid = isValidEmail(email);

        //Validate PhoneNumber
        boolean phoneNumberValid = isValidPhoneNumber(phone);

        return (emailValid || phoneNumberValid) && confirmed;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Set Button
        registerButton.setEnabled(validateFields(emailField.getText().toString(), confirmField.getText().toString(), phoneField.getText().toString()));
    }

    public static UserData getUserData() {
        File file = new File(CustomApplication.getAppContext().getFilesDir(), userdataFilename);
        if(file.exists()) {
            try {
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
                byte[] input = new byte[in.available()];
                in.read(input);
                String string = new String(input);
                UserData userData = gson.fromJson(string, UserData.class);
                if(validateFields(userData.email, userData.email, userData.phoneNumber)) {
                    return userData;
                } else return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else System.out.println("User Data doesnt Exist");

        return null;
    }

    @Override
    public void run() {
        //Declarations
        File file = new File(getFilesDir(), userdataFilename);
        int id = -1;
        UserData userData;
        //BufferedOutputStream out = null;

        //Initialize userID
        try {
            id = IDHandler.getUniqueUserID();
        } catch (IOException e) {
            e.printStackTrace();
            setResult(RESULT_CANCELED);
            finish();
        }

        //Initialize UserData
        userData = new UserData(emailField.getText().toString(), phoneField.getText().toString(), id);

        //Save InstallLog
        InstallLogHandler.handleInstallLog(true);

        //Write Userdata to output stream
        String text = gson.toJson(userData);
        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file, false))) {
            out.write(text.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            setResult(RESULT_CANCELED);
            finish();
        }

        setResult(RESULT_OK);
        finish();
    }

    public class UserData {
        public String email;
        public String phoneNumber;
        public int id;

        public UserData(String email, String phoneNumber, int id) {
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.id = id;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void afterTextChanged(Editable s) {}
}
