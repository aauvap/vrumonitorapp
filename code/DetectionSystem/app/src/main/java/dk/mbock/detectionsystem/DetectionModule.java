package dk.mbock.detectionsystem;

import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;

class DetectionModule {

    private static DetectionModule instance;

    private AppSettings appSettings;

    private long lastScreenChange;

    private float[] accelerationRaw;
    private float[] accelerationMean;
    private float[] jerkRaw;
    private float[] jerkMean;

    private float[] rotationRaw;
    private float[] rotationMean;

    private long lastLinAccReport;

    private long jerkTrigger;
    private long rotationTrigger;

    private long lastAccident;

    private Runnable listener;

    private ArrayList<SavedData> accData = new ArrayList<>();
    private ArrayList<SavedData> gyroData = new ArrayList<>();

    private Thread saveDataThread;

    private int logDuration  = 45000;

    private boolean screenOn = false;

    private float minAcc = 0.05f;
    private boolean runAlgorithm = false;

    private DecimalFormat form;

    public DetectionModule(AppSettings appSettings, Runnable listener) {
        this.appSettings = appSettings;
        this.listener = listener;

        initializeDecimalFormat();

        setup();
        instance = this;
    }

    private void initializeDecimalFormat() {
        DecimalFormatSymbols symb = new DecimalFormatSymbols();
        symb.setDecimalSeparator(',');
        form = new DecimalFormat();
        form.setMaximumFractionDigits(10);
        form.setDecimalFormatSymbols(symb);
    }

    private void setup() {
        int len = Math.max(
                Math.max(
                        appSettings.accelerationArrayLength,
                        appSettings.jerkArrayLength),
                Math.max(
                        appSettings.accelerationMovingAverage,
                        appSettings.jerkMovingAverage));

        accelerationRaw = new float[len];
        accelerationMean = new float[len];
        jerkRaw = new float[len];
        jerkMean = new float[len];

        len = Math.max(
                appSettings.rotationArrayLength,
                appSettings.rotationMovingAverage);

        rotationRaw = new float[len];
        rotationMean = new float[len];

        lastScreenChange = 0;
        lastLinAccReport = 0;
        jerkTrigger = 0;
        rotationTrigger = 0;
        lastAccident = 0;

        screenOn = ((PowerManager) CustomApplication.getAppContext().getSystemService(Context.POWER_SERVICE)).isScreenOn();
    }

    public static void updateAppSettings(AppSettings appSettings) {
        if(instance != null) {
            instance.appSettings = appSettings;

            instance.setup();
        }
    }

    private boolean hasScreenChanged() {
        long res = lastScreenChange+appSettings.screenChangeMillis-System.currentTimeMillis();

        return res >= 0;
    }

    private float calcMag(float[] values) throws Exception {
        if(values.length != 3) {
            throw new Exception("CalcMag: Invalid number of elements.");
        }

        return (float) Math.sqrt(values[0]*values[0]+values[1]*values[1]+values[2]*values[2]);
    }

    public void reportScreenChange(boolean screenOn) {
        this.screenOn = screenOn;
        lastScreenChange = System.currentTimeMillis();
    }

    public synchronized void reportAccelerationMeasurement(float[] values, long time) throws Exception {
        //Save Data
        SavedData savedData = new SavedData(time, values, new Date(System.currentTimeMillis()), screenOn);
        accData.add(savedData);
        SavedData[] sdArray = new SavedData[accData.size()];
        sdArray = accData.toArray(sdArray);

        float magnitude = calcMag(values);
        runAlgorithm = false;
        //Remove old data
        for(SavedData sd : sdArray) {
            if(sd.time < (time - (logDuration*2))) {
                accData.remove(sd);
            } else {
                if(magnitude > minAcc) {
                    runAlgorithm = true;
                    break;
                }
            }
        }

        if(runAlgorithm) {
            //Declarations
            float accSum = magnitude;
            float jerkSum = 0;
            int accTriggers = 0;
            int jerkTriggers = 0;
            boolean triggered = false;

            //Move elements one index back
            for(int i = accelerationRaw.length-1; i > 0; i--) {
                accelerationRaw[i] = accelerationRaw[i-1];
                accelerationMean[i] = accelerationMean[i-1];
                jerkRaw[i] = jerkRaw[i-1];
                jerkMean[i] = jerkMean[i-1];
            }

            //Insert new value on index 0
            accelerationRaw[0] = magnitude;

            //Count up, and then divide sum
            for (int i = 0; i < appSettings.accelerationMovingAverage; i++) {
                accSum += accelerationRaw[i];
            }
            accelerationMean[0] = accSum / appSettings.accelerationMovingAverage;

            //Jerk Calculations
            if(lastLinAccReport > 0) {
                //Declaration
                long msDifference = time - lastLinAccReport;

                //Insert new value on index 0
                jerkRaw[0] = (accelerationRaw[0] - accelerationRaw[1])*1000 / msDifference;

                //Count up, and then divide sum
                for (int i = 0; i < appSettings.jerkMovingAverage; i++) {
                    jerkSum += jerkRaw[i];
                }
                jerkMean[0] = jerkSum / appSettings.jerkMovingAverage;
            }

            //Count Acc Triggers
            for (int i = 0; i < appSettings.accelerationArrayLength; i++) {
                if(accelerationMean[i] > appSettings.accelerationThreshold) {
                    accTriggers++;
                }
            }

            //If enough triggers, a check is triggered
            if(accTriggers >= appSettings.accelerationCheckMin) {
                triggered = true;
            }

            //Count Jerk Triggers
            for (int i = 0; i < appSettings.jerkArrayLength; i++) {
                if(jerkMean[i] > appSettings.jerkThreshold) {
                    jerkTriggers++;
                }
            }

            //If enough triggers, a check is triggered
            if(jerkTriggers >= appSettings.jerkCheckMin) {
                jerkTrigger = time;
                triggered = true;
            }

            if(triggered) {
                checkAccident(time);
            }

            lastLinAccReport = time;
        }
    }

    public synchronized void reportRotationMeasurement(float[] values, long time) throws Exception {
        //Save Data
        SavedData savedData = new SavedData(time, values, new Date(System.currentTimeMillis()), screenOn);
        gyroData.add(savedData);
        SavedData[] sdArray = new SavedData[gyroData.size()];
        sdArray = gyroData.toArray(sdArray);

        //Remove old data
        for(SavedData sd : sdArray) {
            if(sd.time < (time - logDuration*2)) {
                gyroData.remove(sd);
            } else break;
        }

        if(runAlgorithm) {
            float magnitude = calcMag(values);
            float sum = 0;
            int triggerNum = 0;

            for(int i = accelerationRaw.length-1; i > 0; i--) {
                rotationRaw[i] = rotationRaw[i-1];
                rotationMean[i] = rotationMean[i-1];
            }

            rotationRaw[0] = magnitude;

            for(int i = 0; i < appSettings.rotationMovingAverage; i++) {
                sum += rotationRaw[i];
            }
            rotationMean[0] = sum / appSettings.rotationMovingAverage;

            for(int i = 0; i < appSettings.rotationArrayLength; i++) {
                if(rotationMean[i] > appSettings.rotationThreshold) {
                    triggerNum++;
                }
            }

            if(triggerNum >= appSettings.rotationCheckMin) {
                rotationTrigger = time;
                checkAccident(time);
            }
        }

    }

    private void checkAccident(final long time) {
        if(!hasScreenChanged() && System.currentTimeMillis() - lastAccident > 10000) {
            lastAccident = System.currentTimeMillis();
            if(jerkTrigger-lastAccident < appSettings.jerkMillis || rotationTrigger-lastAccident < appSettings.rotationMillis) {

                //Declarations
                final String dateLine = new Date(System.currentTimeMillis()).toString();

                //TODO Debug Code
                System.out.println("Accident Detected");

                //An accident has been triggered. Time to see if it holds up
                final Thread screenChangeThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Save last Screen change
                        long screenChange = lastScreenChange;

                        //Wait and see if screen changes
                        try {
                            Thread.sleep(appSettings.screenChangeMillis);
                        } catch (InterruptedException ignored) {}

                        //Check to see if screen has changed
                        if(lastScreenChange <= screenChange) {
                            //Fully Realized Accident
                            new Thread(listener).start();
                        } else {
                            saveDataThread.interrupt(); //TODO Need a better solution for this
                        }
                    }
                });
                screenChangeThread.start();

                saveDataThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Wait 30 seconds, before saving data
                        try {
                            Thread.sleep(logDuration);
                        } catch (InterruptedException e) {
                            return;
                        }
                        System.out.println("Saving Accident");
                        //Declarations
                        String manufacturer = Build.MANUFACTURER;
                        String model = Build.MODEL;
                        int androidVersion = Build.VERSION.SDK_INT;
                        SettingsActivity.UserData userData = SettingsActivity.getUserData();

                        //Cache arrays
                        SavedData[] accArray  = new SavedData[accData.size()];
                        SavedData[] gyroArray = new SavedData[gyroData.size()];
                        accArray = accData.toArray(accArray);
                        gyroArray = gyroData.toArray(gyroArray);

                        //Fetch Accident ID
                        int accidentID = IDHandler.getUniqueAccidentID();

                        //Declare Metaline
                        String metaLine = new StringBuilder()
                                .append(dateLine).append(";")
                                .append(time).append(";")
                                .append(userData.id).append(";")
                                .append(accidentID).append(";")
                                .append(appSettings.version).append(";")
                                .append(userData.email).append(";")
                                .append(userData.phoneNumber).append(";")
                                .append(androidVersion).append(";")
                                .append(manufacturer).append(";")
                                .append(model).append(";")
                                .append("\n").toString();

                        StringBuilder accLines = new StringBuilder();
                        StringBuilder gyroLines = new StringBuilder();

                        for (int i = 0; i < accArray.length; i++) {
                            SavedData sd = accArray[i];
                            float[] accVal = sd.values;
                            accLines
                                .append(userData.id).append(";")
                                .append(accidentID).append(";")
                                .append(sd.time).append(";")
                                .append(form.format(accVal[0])).append(";")
                                .append(form.format(accVal[1])).append(";")
                                .append(form.format(accVal[2])).append(";")
                                .append(sd.screenOn).append("\n");
                        }

                        for (int i = 0; i < gyroArray.length; i++) {
                            SavedData sd = gyroArray[i];
                            float[] gyroVal = sd.values;

                            gyroLines
                                .append(userData.id).append(";")
                                .append(accidentID).append(";")
                                .append(sd.time).append(";")
                                .append(form.format(gyroVal[0])).append(";")
                                .append(form.format(gyroVal[1])).append(";")
                                .append(form.format(gyroVal[2])).append(";")
                                .append(sd.screenOn).append("\n");
                        }

                        Context context = CustomApplication.getAppContext();

                        //Save Lines
                        try {
                            saveDataToFile(context.getString(R.string.system_accFile), accLines.toString());
                            saveDataToFile(context.getString(R.string.system_gyroFile), gyroLines.toString());
                            saveDataToFile(context.getString(R.string.system_accidentListFile), metaLine);
                        } catch (InterruptedException e) {
                            System.out.println("Save Failed!!!");
                        }
                    }
                });
                saveDataThread.start();
            }
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    private boolean saveDataToFile(String fileName, String data) throws InterruptedException {
        System.out.println("saveDataToFile");
        File file = new File(CustomApplication.getAppContext().getFilesDir(), fileName);
        System.out.println(file.getAbsolutePath());

        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file, true));
            stream.write(data.getBytes());
            stream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private class SavedData {
        public long time;
        public float[] values;
        public Date date;
        public int screenOn;

        public SavedData(long time, float[] values, Date date, boolean screenOn) {
            this.time = time;
            this.date = date;
            this.screenOn = screenOn ? 1 : 0;

            this.values = new float[values.length];
            System.arraycopy(values, 0, this.values, 0, values.length);
        }
    }
}
