package dk.mbock.detectionsystem;

import android.app.Application;
import android.content.Context;

/**
 * Created by mbch on 29/03/16, for the InDev VRU PROJECT
 */
public class CustomApplication extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        CustomApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return CustomApplication.context;
    }
}
