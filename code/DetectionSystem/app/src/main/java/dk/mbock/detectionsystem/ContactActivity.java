package dk.mbock.detectionsystem;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        final Handler handler = new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String idOutput = "";
                try {
                    idOutput = ""+IDHandler.getUniqueUserID();
                } catch (IOException e) {
                    idOutput = "ERROR";
                    e.printStackTrace();
                }
                final String finalIdOutput = idOutput;
                final TextView idTextView = (TextView) findViewById(R.id.contact_idField);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        idTextView.setText(finalIdOutput);
                    }
                });
            }
        }).start();
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
