package dk.mbock.detectionsystem;

import android.content.Context;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by mbch on 17/03/16, for the InDev VRU PROJECT
 */
class FTPConnection {

    /**
     * Uploads a file to the defined FTP server
     * @param file The file to upload
     * @return True if successful
     */
    public static boolean uploadFile(File file, String saveAs) throws JSchException, SftpException, IOException {
        JSch jsch = new JSch();
        Context context = CustomApplication.getAppContext();

        Session session = jsch.getSession(context.getString(R.string.ftp_username), context.getString(R.string.ftp_url));
        session.setPassword(context.getString(R.string.ftp_password));

        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        session.connect();

        Channel channel = session.openChannel( "sftp" );
        channel.connect();

        ChannelSftp sftpChannel = (ChannelSftp) channel;

        String dst = "vruData/" + saveAs;
        System.err.println(dst);
        sftpChannel.put(new FileInputStream(file), dst, ChannelSftp.APPEND);

        dst = "vruData/uploadLog.txt";
        sftpChannel.put("Test: " + new Date(System.currentTimeMillis()).toString(), dst, ChannelSftp.APPEND);

        sftpChannel.exit();
        session.disconnect();

        return true;
    }
}
