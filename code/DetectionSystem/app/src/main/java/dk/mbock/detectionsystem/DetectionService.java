package dk.mbock.detectionsystem;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class DetectionService extends Service implements SensorEventListener, Runnable{

    public static boolean running = false;

    private Sensor acc;
    private Sensor gyro;

    private DetectionModule detectionModule;

    private static Handler handler;

    private PowerManager powerManager;

    private boolean isScreenOn;

    private int notificationID = 1337;

    private Context context;

    private float[] lastAcc  = null;
    private float[] lastGyro = null;

    private static boolean hasWIFI = true;
    private static DetectionService thisService;

    private static Runnable runnable;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        running = true;

        context = this;

        System.out.println("Service Started");
        thisService = this;

        handler = new Handler();

        Intent startMainActivityIntent = new Intent(thisService, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(thisService, 0, startMainActivityIntent, PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new NotificationCompat.Builder(thisService)
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(getString(R.string.notification_content))
                .setContentIntent(pendingIntent)
                .build();

        //startForeground(notificationID, notification);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, notification);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                AppSettings appSettings = new WebInterfaceConnection().getAppSettings(context);
                if(appSettings == null) {
                    CreateToast("Internet Connection Needed, restart to try again");
                    stopSelf();
                    return;
                }
                detectionModule = new DetectionModule(appSettings, new Runnable() {
                    @Override
                    public void run() {
                        notificationID = 1337;
                    }
                });
                startReading();

                powerManager = (PowerManager) getSystemService(POWER_SERVICE);

                handler.post(thisService);


            }
        });

        int restartInterval = 1000*60*60*6;
        Intent intent1 = new Intent(this, RestartService.class);
        pendingIntent = PendingIntent.getService(this, 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC, restartInterval, restartInterval, pendingIntent);

        thread.start();

        return START_STICKY;
    }


    public static void wifiChanged(boolean wifi) {
        hasWIFI = wifi;

        if(!wifi) {
            handler.post(thisService);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void run() {
        boolean screenOn = powerManager.isScreenOn();
        if(screenOn != isScreenOn) {
            isScreenOn = powerManager.isScreenOn();
            detectionModule.reportScreenChange(screenOn);
        }

        final long time = System.currentTimeMillis();
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(lastAcc != null){
                    try {
                        detectionModule.reportAccelerationMeasurement(lastAcc, time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(lastGyro != null) {
                    try {
                        detectionModule.reportRotationMeasurement(lastGyro, time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                lastAcc = null;
                lastGyro = null;
            }
        }).start();
        if(!hasWIFI) {
            handler.postDelayed(this, 100);
        }
    }

    private void startReading() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        acc  = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        int sensorDelay = SensorManager.SENSOR_DELAY_NORMAL;

        sensorManager.registerListener(this, acc, sensorDelay, 100);
        sensorManager.registerListener(this, gyro, sensorDelay, 100);
    }

    private void stopReading() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if(!hasWIFI) {
            if (event.sensor == acc) {
                lastAcc = event.values;
            } else if (event.sensor == gyro) {
                lastGyro = event.values;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        running = false;
        stopReading();
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancel(notificationID);
    }

    private void CreateToast(final String toastText) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, toastText, duration);
                toast.show();
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
