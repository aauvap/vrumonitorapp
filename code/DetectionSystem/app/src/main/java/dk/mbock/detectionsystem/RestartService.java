package dk.mbock.detectionsystem;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class RestartService extends Service {
    public RestartService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!DetectionService.running) {
            startActivity(new Intent(CustomApplication.getAppContext(), DetectionService.class));
        }

        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }
}
