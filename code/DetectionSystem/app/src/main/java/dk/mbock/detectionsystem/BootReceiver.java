package dk.mbock.detectionsystem;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {

    private static PendingIntent pendingIntent;
    public static PendingIntent getPendingIntent(Context context) {
        if(pendingIntent == null) {
            initializePendingIntent(context);
        }

        return pendingIntent;
    }

    private static void initializePendingIntent(Context context) {
        Intent uploadIntent = new Intent(context, UploadService.class);
        pendingIntent = PendingIntent.getService(context, 0, uploadIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent newIntent = new Intent(context, DetectionService.class);
        context.startService(newIntent);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        alarmManager.setInexactRepeating(AlarmManager.RTC, 0, 24*60*60*1000, pendingIntent);
        System.out.println("Boot Received");
    }
}
