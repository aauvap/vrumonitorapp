package dk.mbock.detectionsystem;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.io.File;
import java.io.IOException;

public class UploadService extends Service implements Runnable {

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    //Declarations
    private File
            accFilePath,
            gyroFilePath,
            accidentListFilePath,
            errorListFilePath;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Context context = CustomApplication.getAppContext();

        if(isWIFIConnected()) {

            //Set Variables
            accFilePath = new File(context.getFilesDir(), getString(R.string.system_accFile));
            gyroFilePath= new File(context.getFilesDir(), getString(R.string.system_gyroFile));
            accidentListFilePath = new File(context.getFilesDir(), getString(R.string.system_accidentListFile));
            errorListFilePath = new File(context.getFilesDir(), getString(R.string.system_errorListFile));

            //Start the thread
            Thread thisThread = new Thread(this);
            thisThread.start();

        } else stopSelf();

        return START_NOT_STICKY;
    }

    /**
     * Attempts to upload a given file to the defined SFTP server. Will try 10 times. If successful, deletes the local version of the file.
     * @param file The file to be uploaded.
     */
    private void uploadFile(File file) {
        boolean success = false;
        System.out.println("File Path: " + file);
        for(int i = 0; i < 10 && !success && file.exists(); i++) {
            System.out.println("Attempting upload. Name: " + file.getName());
            try {
                if(FTPConnection.uploadFile(file, file.getName())) {
                    System.out.println("Upload Complete");
                    success = true;

                    while(!file.delete() && file.exists()) {Thread.yield();}
                    System.out.println("Deletion Complete");
                }
            } catch (JSchException | SftpException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run()  {
        System.out.println("Begin Upload Run");

        //Upload the different files
        uploadFile(accFilePath);
        uploadFile(gyroFilePath);
        uploadFile(accidentListFilePath);
        uploadFile(errorListFilePath);

        //Update Appsettings
        AppSettings appSettings = new WebInterfaceConnection().getAppSettings(this);
        DetectionModule.updateAppSettings(appSettings);

        //Stop the service
        stopSelf();
    }

    private boolean isWIFIConnected() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        System.out.println("WIFI: " + mWifi.isConnected());

        return mWifi.isConnected();
    }
}
