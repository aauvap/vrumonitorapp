package dk.mbock.detectionsystem;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by mbch on 04/05/16, for the InDev VRU PROJECT
 */
public class InstallLogHandler {

    private static SensorManager sensorManager;

    public static void handleInstallLog(final boolean success) {
        Context context = CustomApplication.getAppContext();

        final File file = new File(context.getFilesDir(), context.getString(R.string.main_installLogLocation));
        sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                if(!file.exists()) {
                    saveAndUpload(file, success);
                }
            }
        }).start();
    }

    private static void saveAndUpload(File file, boolean success) {
        boolean hasAcc = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null,
                hasGyro= sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null;
        String  manufacturer = Build.MANUFACTURER,
                model = Build.MODEL,
                date = new Date(System.currentTimeMillis()).toString();
        int     androidVersion = Build.VERSION.SDK_INT,
                id = -1;

        if(success) {
            try {
                id = IDHandler.getUniqueUserID();
            } catch (IOException ignored) {}
        }

        String string2Write = "" +
                date + ";" +
                success + ";" +
                id + ";" + 
                manufacturer + ";" +
                model + ";" +
                androidVersion + ";" +
                hasAcc + ";" +
                hasGyro + "\n";
        BufferedOutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(file));
            out.write(string2Write.getBytes());
            out.flush();

            FTPConnection.uploadFile(file, file.getName());
        } catch (IOException | SftpException | JSchException e) {
            e.printStackTrace();
        } finally {
            if(out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
