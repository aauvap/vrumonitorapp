package dk.mbock.detectionsystem;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mbch on 29/03/16, for the InDev VRU PROJECT
 */
@SuppressWarnings("FieldCanBeLocal")
class IDHandler {

    private static final String userFileName = "userid.txt";
    private static int uniqueUserID = -1;
    private static String userURL = "http://vrudetector.create.aau.dk/data/userID.php";
    private static WebInterfaceConnection webInterfaceConnection;
    private static Context context;

    private static void init() {
        //Initialize webinterfaceconnection, if null
        if(webInterfaceConnection == null) {
            webInterfaceConnection = new WebInterfaceConnection();
        }

        //Initialize context if null
        if(context == null) {
            context = CustomApplication.getAppContext();
        }
    }


    public static int getUniqueUserID() throws IOException {
        init();

        //Check if id already exists in ram
        if(uniqueUserID >= 0) return uniqueUserID;

        //Check if id already exists on device
        File file = new File(context.getFilesDir(), userFileName);
        if(file.exists()) {
            try {
                BufferedInputStream in = new BufferedInputStream(context.openFileInput(userFileName));
                byte[] buffer = new byte[in.available()];
                in.read(buffer);
                String string = new String(buffer);
                uniqueUserID = Integer.parseInt(string);
                in.close();
                return uniqueUserID;
            } catch (IOException e) {
                System.out.println("IO EXCEPTION In FILE RETRIEVAL");
            }
        }

        //Else, request new id and return that
        String response = webInterfaceConnection.postTextFromURL(userURL, new String[]{"password"}, new String[]{context.getString(R.string.passkey)});
        uniqueUserID = Integer.parseInt(response);
        try (BufferedOutputStream out = new BufferedOutputStream(context.openFileOutput(userFileName, Context.MODE_PRIVATE))) {
            out.write(("" + uniqueUserID).getBytes());
        }

        System.out.println("Unique User ID: " + uniqueUserID);
        return uniqueUserID;
    }

    public static int getUniqueAccidentID(){
        init();

        File file = new File(context.getFilesDir(), context.getString(R.string.system_accidentIDFile));
        int res = -1;

        if(file.exists()) {
            BufferedInputStream in = null;
            try {
                in = new BufferedInputStream(new FileInputStream(file));
                byte[] buf = new byte[in.available()];
                in.read(buf);
                String string = new String(buf);
                res = Integer.parseInt(string)+1;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            res = 0;
        }

        if(res >= 0) {
            BufferedOutputStream out = null;
            try {
                out = new BufferedOutputStream(new FileOutputStream(file));
                out.write(("" + res).getBytes());
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }

        return res;
    }
}
