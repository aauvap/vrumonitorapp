package dk.mbock.detectionsystem;

/**
 * Created by mbch on 09/02/16, for the InDev VRU PROJECT
 */


public class AppSettings {
    public int version;
    public long screenChangeMillis;

    public int accelerationArrayLength;
    public int accelerationMovingAverage;
    public int accelerationCheckMin;
    public float accelerationThreshold;
    public int jerkArrayLength;
    public int jerkMovingAverage;
    public int jerkCheckMin;
    public float jerkThreshold;

    public int rotationArrayLength;
    public int rotationMovingAverage;
    public int rotationCheckMin;
    public float rotationThreshold;

    public int jerkMillis;
    public int rotationMillis;
}
