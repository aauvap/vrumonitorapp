package dk.mbock.longrunningtest;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class MyService extends Service implements Runnable {
    public MyService() {

    }

    public final static String EXTRA_TYPE = "MyServiceExtra";

    private Handler handler;
    private final long delay = 1000;
    public static final String fileName = "longRunning";
    private final String header = "type, date, timestamp";
    private final int NOTIFICATION_ID = 1337;

    public static boolean running = false;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        handler.postDelayed(this, delay);


        Intent newIntent = new Intent(getBaseContext(), MyService.class);
        newIntent.putExtra(EXTRA_TYPE, "exceptionRestart");

        final PendingIntent pendingIntent = PendingIntent.getService(getBaseContext(), 1, newIntent, flags);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, 2000, pendingIntent);
                System.exit(2);
            }
        });

        saveDataToFile(fileName, "\n" + intent.getStringExtra(EXTRA_TYPE) + "," + timeString(), header);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("LongRunning")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentText("Running")
                .build();

        startForeground(NOTIFICATION_ID, notification);

        running = true;

        return START_STICKY;
    }

    @Override
    public void run() {
        saveDataToFile(fileName, "\n,"+timeString(), header);
        handler.postDelayed(this, delay);
    }

    private boolean saveDataToFile(String fileName, String data, String header) {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            return false;
        }

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName+".txt");

        String text2write = "";
        if(!file.exists()) {
            text2write = header;
        }
        text2write += data;

        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file, true));
            stream.write(text2write.getBytes());
            stream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        running = false;
        try {
            handler.removeCallbacks(this);
        } catch (NullPointerException ignored) {}
    }

    public String timeString() {
        long currentTime = System.currentTimeMillis();
        return new Date(currentTime).toString() + "," + currentTime;
    }
}