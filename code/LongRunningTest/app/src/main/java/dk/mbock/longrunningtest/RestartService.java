package dk.mbock.longrunningtest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class RestartService extends Service {
    public static boolean active;

    public RestartService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(active) {
            if(!MyService.running) {
                Intent restartMyservice = new Intent(this, MyService.class);
                restartMyservice.putExtra(MyService.EXTRA_TYPE, "Restart");
                startService(restartMyservice);
            }

            PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, flags);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, 60000, pendingIntent);
        }

        stopSelf();

        return super.onStartCommand(intent, flags, startId);
    }
}
