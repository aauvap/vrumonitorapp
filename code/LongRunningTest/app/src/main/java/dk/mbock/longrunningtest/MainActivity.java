package dk.mbock.longrunningtest;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity implements Runnable {

    private TextView textView;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent startServiceIntent = new Intent(this, MyService.class);
        startServiceIntent.putExtra(MyService.EXTRA_TYPE, "Start");
        textView = (TextView) findViewById(R.id.helloText);

        handler = new Handler();
        handler.post(this);

        findViewById(R.id.stopButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(context, MyService.class));
                RestartService.active = false;
            }
        });

        findViewById(R.id.startButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MyService.running) {
                    startService(startServiceIntent);

                    PendingIntent pendingIntent = PendingIntent.getService(context, 0, new Intent(context, RestartService.class), PendingIntent.FLAG_ONE_SHOT);
                    RestartService.active = true;
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC, 60000, pendingIntent);
                }
            }
        });

        findViewById(R.id.clearButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearData();
            }
        });

        findViewById(R.id.fakeStop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(context, MyService.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void clearData() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == DialogInterface.BUTTON_POSITIVE) {
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), MyService.fileName+".txt");
                    String resultText = "";
                    if(file.delete()) {
                        resultText = "Clear Successful";
                    } else {
                        resultText = "Could not be cleared (might already have been deleted";
                    }

                    final String finalString = resultText;

                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(finalString);
                        }
                    });
                }
            }
        };

        new AlertDialog.Builder(this)
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener)
                .show();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //restart();
    }

    @Override
    public void run() {
        textView.post(new Runnable() {
            @Override
            public void run() {
                textView.setText(MyService.running ? "Service Running" : "Service Stopped");
            }
        });

        handler.postDelayed(this, 100);
    }
}