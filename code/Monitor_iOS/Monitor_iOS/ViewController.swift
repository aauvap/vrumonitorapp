//
//  ViewController.swift
//  Monitor_iOS
//
//  Created by Mads Bock Christensen on 11/08/16.
//  Copyright © 2016 AAUVAP. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {
    @IBOutlet weak var labelX: UILabel!
    @IBOutlet weak var labelY: UILabel!
    @IBOutlet weak var labelZ: UILabel!
    
    var motionManager : CMMotionManager!
    var i = 0
    var started = false
    var timer : NSTimer!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        labelX.text = "Is X"
        labelY.text = "Is Y"
        labelZ.text = "Is Z"
        
        motionManager = CMMotionManager()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "loop", userInfo: nil, repeats: true)
    }
    
    func loop(){
        if let accData = motionManager.accelerometerData {
            labelX.text = "X: " + String(accData.acceleration.x)+String(i)
            labelY.text = "Y: " + String(accData.acceleration.y)
            labelZ.text = "Z: " + String(accData.acceleration.z)
            
            i = i+1
        }
    }
    @IBAction func onButtonClick(sender: UIButton) {
        if(started) {
            sender.setTitle("Click here to start", forState: UIControlState.Normal)
            motionManager.stopAccelerometerUpdates()
            timer.invalidate()
        } else {
            sender.setTitle("Click here to stop", forState: UIControlState.Normal)
            motionManager.startAccelerometerUpdates()
            timer.fire()
        }
        started = !started
    }
    
    override func viewDidDisappear(animated: Bool) {
        motionManager.stopAccelerometerUpdates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

