# VRUMonitor #

VRUMonitor is an app for collecting statistics about traffic accidents experienced by vulnerable road users. See [administrative/VRUMonitor-Preliminaryprojectdescription.pdf](administrative/VRUMonitor-Preliminaryprojectdescription.pdf) for a more thorough description.