-- phpMyAdmin SQL Dump
-- version 4.4.14.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 16, 2015 at 11:47 AM
-- Server version: 5.6.25-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vruMonitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `date_answers`
--

CREATE TABLE IF NOT EXISTS `date_answers` (
  `id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL,
  `date_field_id` int(11) NOT NULL,
  `datetime` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `date_fields`
--

CREATE TABLE IF NOT EXISTS `date_fields` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `min_date` date DEFAULT NULL,
  `max_date` date DEFAULT NULL,
  `allow_time` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `free_text_answers`
--

CREATE TABLE IF NOT EXISTS `free_text_answers` (
  `id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL,
  `free_text_field_id` int(11) NOT NULL,
  `text` varchar(2000) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `free_text_fields`
--

CREATE TABLE IF NOT EXISTS `free_text_fields` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `sample_text` varchar(500) DEFAULT NULL,
  `min_length` int(11) DEFAULT NULL,
  `max_length` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE IF NOT EXISTS `incidents` (
  `id` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `detected_mode_of_transportation` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `map_answers`
--

CREATE TABLE IF NOT EXISTS `map_answers` (
  `id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL,
  `map_field_id` int(11) NOT NULL,
  `latitude` int(11) DEFAULT NULL,
  `longitude` int(11) DEFAULT NULL,
  `orientation` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `map_fields`
--

CREATE TABLE IF NOT EXISTS `map_fields` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `min_num_marks` int(11) DEFAULT NULL,
  `max_num_marks` int(11) DEFAULT NULL,
  `map_bounding_box` varchar(100) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_answers`
--

CREATE TABLE IF NOT EXISTS `multiple_choice_answers` (
  `id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL,
  `multiple_choice_option_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_fields`
--

CREATE TABLE IF NOT EXISTS `multiple_choice_fields` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `min_num_choices` int(11) DEFAULT NULL,
  `max_num_choices` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_options`
--

CREATE TABLE IF NOT EXISTS `multiple_choice_options` (
  `id` int(11) NOT NULL,
  `multiple_choice_field_id` int(11) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `picture_dir` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `number_answers`
--

CREATE TABLE IF NOT EXISTS `number_answers` (
  `id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL,
  `number_field_id` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `float_number` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `number_fields`
--

CREATE TABLE IF NOT EXISTS `number_fields` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `min_number` int(11) DEFAULT NULL,
  `max_number` int(11) DEFAULT NULL,
  `allow_floats` tinyint(1) DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE IF NOT EXISTS `participants` (
  `id` int(11) NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `language` varchar(2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `questionnaires`
--

CREATE TABLE IF NOT EXISTS `questionnaires` (
  `id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `intro_text` varchar(1000) DEFAULT NULL,
  `end_text` varchar(1000) DEFAULT NULL,
  `language` varchar(2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `question_sequences`
--

CREATE TABLE IF NOT EXISTS `question_sequences` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `to_question_id` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sensor_dumps`
--

CREATE TABLE IF NOT EXISTS `sensor_dumps` (
  `id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL,
  `data` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sequence_and_conditions`
--

CREATE TABLE IF NOT EXISTS `sequence_and_conditions` (
  `id` int(11) NOT NULL,
  `question_sequence_id` int(11) NOT NULL,
  `multiple_choice_option_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sequence_or_conditions`
--

CREATE TABLE IF NOT EXISTS `sequence_or_conditions` (
  `id` int(11) NOT NULL,
  `question_sequence_id` int(11) NOT NULL,
  `multiple_choice_option_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `date_answers`
--
ALTER TABLE `date_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_date_answers_incidents1_idx` (`incident_id`),
  ADD KEY `fk_date_answers_date_fields1_idx` (`date_field_id`);

--
-- Indexes for table `date_fields`
--
ALTER TABLE `date_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dateFields_questions1_idx` (`question_id`);

--
-- Indexes for table `free_text_answers`
--
ALTER TABLE `free_text_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_free_text_answers_incidents1_idx` (`incident_id`),
  ADD KEY `fk_free_text_answers_free_text_fields1_idx` (`free_text_field_id`);

--
-- Indexes for table `free_text_fields`
--
ALTER TABLE `free_text_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_freeTextFields_questions1_idx` (`question_id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_incidents_participants1_idx` (`participant_id`);

--
-- Indexes for table `map_answers`
--
ALTER TABLE `map_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_map_answers_mapFields1_idx` (`map_field_id`),
  ADD KEY `fk_map_answers_incidents1_idx` (`incident_id`);

--
-- Indexes for table `map_fields`
--
ALTER TABLE `map_fields`
  ADD PRIMARY KEY (`id`,`question_id`),
  ADD KEY `fk_mapFields_questions1_idx` (`question_id`);

--
-- Indexes for table `multiple_choice_answers`
--
ALTER TABLE `multiple_choice_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_multiple_choice_answers_incidents1_idx` (`incident_id`),
  ADD KEY `fk_multiple_choice_answers_multiple_choice_options1_idx` (`multiple_choice_option_id`);

--
-- Indexes for table `multiple_choice_fields`
--
ALTER TABLE `multiple_choice_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_multiple_choice_fields_questions_idx` (`question_id`);

--
-- Indexes for table `multiple_choice_options`
--
ALTER TABLE `multiple_choice_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_answerOptions_questions_idx` (`multiple_choice_field_id`);

--
-- Indexes for table `number_answers`
--
ALTER TABLE `number_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_number_answers_incidents1_idx` (`incident_id`),
  ADD KEY `fk_number_answers_number_fields1_idx` (`number_field_id`);

--
-- Indexes for table `number_fields`
--
ALTER TABLE `number_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_numberFields_questions1_idx` (`question_id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionnaires`
--
ALTER TABLE `questionnaires`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_questions_questionnaires1_idx` (`questionnaire_id`);

--
-- Indexes for table `question_sequences`
--
ALTER TABLE `question_sequences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_question_sequence_questions1_idx` (`question_id`),
  ADD KEY `fk_question_sequence_questions2` (`to_question_id`);

--
-- Indexes for table `sensor_dumps`
--
ALTER TABLE `sensor_dumps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sensor_dumps_incidents1_idx` (`incident_id`);

--
-- Indexes for table `sequence_and_conditions`
--
ALTER TABLE `sequence_and_conditions`
  ADD PRIMARY KEY (`id`,`multiple_choice_option_id`),
  ADD KEY `fk_sequence_and_conditions_question_sequence1_idx` (`question_sequence_id`),
  ADD KEY `fk_sequence_and_conditions_multiple_choice_options1_idx` (`multiple_choice_option_id`);

--
-- Indexes for table `sequence_or_conditions`
--
ALTER TABLE `sequence_or_conditions`
  ADD PRIMARY KEY (`id`,`multiple_choice_option_id`),
  ADD KEY `fk_sequence_or_conditions_question_sequence1_idx` (`question_sequence_id`),
  ADD KEY `fk_sequence_or_conditions_multiple_choice_options1_idx` (`multiple_choice_option_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `date_answers`
--
ALTER TABLE `date_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `date_fields`
--
ALTER TABLE `date_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `free_text_fields`
--
ALTER TABLE `free_text_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `map_answers`
--
ALTER TABLE `map_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `map_fields`
--
ALTER TABLE `map_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `multiple_choice_answers`
--
ALTER TABLE `multiple_choice_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `multiple_choice_fields`
--
ALTER TABLE `multiple_choice_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `multiple_choice_options`
--
ALTER TABLE `multiple_choice_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `number_answers`
--
ALTER TABLE `number_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `number_fields`
--
ALTER TABLE `number_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questionnaires`
--
ALTER TABLE `questionnaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question_sequences`
--
ALTER TABLE `question_sequences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sensor_dumps`
--
ALTER TABLE `sensor_dumps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sequence_and_conditions`
--
ALTER TABLE `sequence_and_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sequence_or_conditions`
--
ALTER TABLE `sequence_or_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `date_answers`
--
ALTER TABLE `date_answers`
  ADD CONSTRAINT `fk_date_answers_date_fields1` FOREIGN KEY (`date_field_id`) REFERENCES `date_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_date_answers_incidents1` FOREIGN KEY (`incident_id`) REFERENCES `incidents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `date_fields`
--
ALTER TABLE `date_fields`
  ADD CONSTRAINT `fk_dateFields_questions1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `free_text_answers`
--
ALTER TABLE `free_text_answers`
  ADD CONSTRAINT `fk_free_text_answers_free_text_fields1` FOREIGN KEY (`free_text_field_id`) REFERENCES `free_text_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_free_text_answers_incidents1` FOREIGN KEY (`incident_id`) REFERENCES `incidents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `free_text_fields`
--
ALTER TABLE `free_text_fields`
  ADD CONSTRAINT `fk_freeTextFields_questions1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `incidents`
--
ALTER TABLE `incidents`
  ADD CONSTRAINT `fk_incidents_participants1` FOREIGN KEY (`participant_id`) REFERENCES `participants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `map_answers`
--
ALTER TABLE `map_answers`
  ADD CONSTRAINT `fk_map_answers_incidents1` FOREIGN KEY (`incident_id`) REFERENCES `incidents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_map_answers_mapFields1` FOREIGN KEY (`map_field_id`) REFERENCES `map_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `map_fields`
--
ALTER TABLE `map_fields`
  ADD CONSTRAINT `fk_mapFields_questions1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `multiple_choice_answers`
--
ALTER TABLE `multiple_choice_answers`
  ADD CONSTRAINT `fk_multiple_choice_answers_incidents1` FOREIGN KEY (`incident_id`) REFERENCES `incidents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_multiple_choice_answers_multiple_choice_options1` FOREIGN KEY (`multiple_choice_option_id`) REFERENCES `multiple_choice_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `multiple_choice_fields`
--
ALTER TABLE `multiple_choice_fields`
  ADD CONSTRAINT `fk_multiple_choice_fields_questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `multiple_choice_options`
--
ALTER TABLE `multiple_choice_options`
  ADD CONSTRAINT `fk_answerOptions_questions` FOREIGN KEY (`multiple_choice_field_id`) REFERENCES `multiple_choice_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `number_answers`
--
ALTER TABLE `number_answers`
  ADD CONSTRAINT `fk_number_answers_incidents1` FOREIGN KEY (`incident_id`) REFERENCES `incidents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_number_answers_number_fields1` FOREIGN KEY (`number_field_id`) REFERENCES `number_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `number_fields`
--
ALTER TABLE `number_fields`
  ADD CONSTRAINT `fk_numberFields_questions1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `fk_questions_questionnaires1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaires` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_sequences`
--
ALTER TABLE `question_sequences`
  ADD CONSTRAINT `fk_question_sequence_questions1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_question_sequence_questions2` FOREIGN KEY (`to_question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sensor_dumps`
--
ALTER TABLE `sensor_dumps`
  ADD CONSTRAINT `fk_sensor_dumps_incidents1` FOREIGN KEY (`incident_id`) REFERENCES `incidents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sequence_and_conditions`
--
ALTER TABLE `sequence_and_conditions`
  ADD CONSTRAINT `fk_sequence_and_conditions_multiple_choice_options1` FOREIGN KEY (`multiple_choice_option_id`) REFERENCES `multiple_choice_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sequence_and_conditions_question_sequence1` FOREIGN KEY (`question_sequence_id`) REFERENCES `question_sequences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sequence_or_conditions`
--
ALTER TABLE `sequence_or_conditions`
  ADD CONSTRAINT `fk_sequence_or_conditions_multiple_choice_options1` FOREIGN KEY (`multiple_choice_option_id`) REFERENCES `multiple_choice_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sequence_or_conditions_question_sequence1` FOREIGN KEY (`question_sequence_id`) REFERENCES `question_sequences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
